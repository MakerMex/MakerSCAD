PK
     {�*IR��J  J     xml<xml xmlns="http://www.w3.org/1999/xhtml"><block type="three_new_basic_scene" id="*ilxST7=J+#SHbWYrR:u" x="34" y="28"><next><block type="three_scene_add" id="06Lk`6bQmUu^9J%n=dBV"><value name="mesh"><shadow type="three_box" id="-lW=SZBz3IJuqL1Q9S.q"><value name="size"><shadow type="mm_xyz" id=")B1!7Se,Vg{lo{SXvN=7"><field name="X">100</field><field name="Y">100</field><field name="Z">100</field></shadow></value><value name="segments"><shadow type="math_number" id="Jaf;d5Z/c6^vgm?XD:g:"><field name="NUM">1</field></shadow></value></shadow><block type="three_part" id="Aib3ZERd4)W=*Yie^tnX"><field name="boolean_op">subtract</field><value name="block1"><shadow type="three_box" id="L(,+,]sAPqAHG=G]f=`W"><value name="size"><shadow type="mm_xyz" id="Jt@X=8r+|9OtTuI~2Qmc"><field name="X">500</field><field name="Y">50</field><field name="Z">500</field></shadow></value><value name="segments"><shadow type="math_number" id="{y.DqhS?o;Wk]1E%wGT="><field name="NUM">1</field></shadow></value></shadow></value><value name="block2"><shadow type="three_sphere" id="b|R.^Ac@I^#oR~:5AjSq"><value name="diameter"><shadow type="math_number" id="-kD0#oQ{6EI6o5Z2L)xr"><field name="NUM">120</field></shadow></value><value name="resolution"><shadow type="math_number" id="n?Q#]0.jkau@hcRqIL;@"><field name="NUM">16</field></shadow></value></shadow><block type="three_rotate" id="*`)Xh+nF`oKRzu7|S)T7"><value name="coordinates"><shadow type="mm_xyz" id="UoSw1Xng/h:PbYWe44J~"><field name="X">0</field><field name="Y">45</field><field name="Z">0</field></shadow></value><value name="object"><shadow type="three_box" id=")bP[iBQ%cj~I}TnDF!ks"><value name="size"><shadow type="mm_xyz" id="/,dZ{.IO+OVa6hA@Kr`W"><field name="X">100</field><field name="Y">100</field><field name="Z">100</field></shadow></value><value name="segments"><shadow type="math_number" id="QTK4Z0D=::Yg2)VTOaLQ"><field name="NUM">1</field></shadow></value></shadow><block type="three_object_loader" id="_F=/rDRYcG2q`f`_)5ss"><field name="id">2ac_516504a92070de6cc3dc7b8e70bb8217</field></block></value></block></value></block></value></block></next></block></xml>PK
     {�*I� J  J     jsmm_scene = null;
mm_scene = new THREE.Scene();
mm_scene.add(new THREE.HemisphereLight('#ffffff', '#666666', 1));
mm_scene.add(mm_boolean_op('subtract', mm_new_mesh(new THREE.BoxGeometry( 500, 50, 500, 1, 1, 1 )), mm_assets_objects["2ac_516504a92070de6cc3dc7b8e70bb8217" /* Cube1.stl */].data.clone()
  .setRotation([0, 45, 0])));
PK
     {�*I�z.c   c   	   functions{"script1.js":{"hash":"0_d41d8cd98f00b204e9800998ecf8427e","name":"script1.js","data":"","url":""}}PK
     {�*I            	   textures/PK
     {�*IC���         textures/metadata.json{}PK
     {�*I               objects/PK
     {�*I�%��F   F      objects/metadata.json{"2ac_516504a92070de6cc3dc7b8e70bb8217":{"name":"Cube1.stl","url":""}}PK
     {�*I)��5    6   objects/2ac_516504a92070de6cc3dc7b8e70bb8217_Cube1.stlo 
v 50 50 50
v 50 -50 50
v 50 50 -50
v 50 -50 50
v 50 -50 -50
v 50 50 -50
v -50 50 -50
v -50 -50 -50
v -50 50 50
v -50 -50 -50
v -50 -50 50
v -50 50 50
v -50 50 -50
v -50 50 50
v 50 50 -50
v -50 50 50
v 50 50 50
v 50 50 -50
v -50 -50 50
v -50 -50 -50
v 50 -50 50
v -50 -50 -50
v 50 -50 -50
v 50 -50 50
v -50 50 50
v -50 -50 50
v 50 50 50
v -50 -50 50
v 50 -50 50
v 50 50 50
v 50 50 -50
v 50 -50 -50
v -50 50 -50
v 50 -50 -50
v -50 -50 -50
v -50 50 -50
vn 1 0 0
vn 1 0 0
vn 1 0 0
vn 1 0 0
vn 1 0 0
vn 1 0 0
vn -1 0 0
vn -1 0 0
vn -1 0 0
vn -1 0 0
vn -1 0 0
vn -1 0 0
vn 0 1 0
vn 0 1 0
vn 0 1 0
vn 0 1 0
vn 0 1 0
vn 0 1 0
vn 0 -1 0
vn 0 -1 0
vn 0 -1 0
vn 0 -1 0
vn 0 -1 0
vn 0 -1 0
vn 0 0 1
vn 0 0 1
vn 0 0 1
vn 0 0 1
vn 0 0 1
vn 0 0 1
vn 0 0 -1
vn 0 0 -1
vn 0 0 -1
vn 0 0 -1
vn 0 0 -1
vn 0 0 -1
f 1//1 2//2 3//3
f 4//4 5//5 6//6
f 7//7 8//8 9//9
f 10//10 11//11 12//12
f 13//13 14//14 15//15
f 16//16 17//17 18//18
f 19//19 20//20 21//21
f 22//22 23//23 24//24
f 25//25 26//26 27//27
f 28//28 29//29 30//30
f 31//31 32//32 33//33
f 34//34 35//35 36//36
PK 
     {�*IR��J  J                   xmlPK 
     {�*I� J  J               k  jsPK 
     {�*I�z.c   c   	             �	  functionsPK 
     {�*I            	            _
  textures/PK 
     {�*IC���                   �
  textures/metadata.jsonPK 
     {�*I                        �
  objects/PK 
     {�*I�%��F   F                �
  objects/metadata.jsonPK 
     {�*I)��5    6             [  objects/2ac_516504a92070de6cc3dc7b8e70bb8217_Cube1.stlPK      �  �    