PK
     �{�J�dv	   	      mmp{"ui":{}}PK
     �{�J�F�@20  20     xml<xml xmlns="http://www.w3.org/1999/xhtml"><block type="three_init" id="KO~TW,u_S:w+e06Z`tji" x="-582" y="-714"><field name="lights">TRUE</field><field name="physijs">FALSE</field><next><block type="bi_named_function" id=")ub:P#.OX+~5hVOg##8N"><field name="function_type">function </field><field name="name">involuteGear</field><field name="args">numTeeth, circularPitch, thickness, hole, twist, pressureAngle, clearance</field><statement name="chain"><block type="mm_var" id="1ON3KO-Bhkvop0M8BC;2"><field name="var">teeth</field><value name="val"><block type="three_new_group" id="S1f_@DI3I;n6_#AEffD1"></block></value><next><block type="mm_script_text" id="Z{Bdjs@Qc@C0e[ZSGo|y"><field name="name">script1.js</field><next><block type="mm_var" id="1RWQ8|F[{E]8zE#ixR0{"><field name="var">tooth</field><value name="val"><block type="three_extrude" id="o5~/NCodj9eEy^NsMC[u"><value name="height"><shadow type="math_number" id="WMYLc%Fkax)_XU5{eBX*"><field name="NUM">10</field></shadow><block type="mm_var_return" id="J!`+*pn^@|S,`EIJ0LNM"><field name="NAME">thickness</field></block></value><value name="segments"><shadow type="math_number" id="hq?+~PKiheg8/i)-u*+j"><field name="NUM">10</field></shadow></value><value name="twist"><shadow type="math_number" id="IFd.!hc5:hM/#]l#z!)M"><field name="NUM">0</field></shadow><block type="mm_var_return" id="*_j1G5S/n+Af(Sx`R1=K"><field name="NAME">twist</field></block></value><value name="polygon"><shadow type="three_polygon" id="UmwbC;DfF:C;_nY9|Al7"><field name="code">[[-3,18],[10,0],[10,12]]</field></shadow><block type="mm_var_return" id="z{)gefZd,M?^z;8E2Uk*"><field name="NAME">toothShape</field></block></value></block></value><next><block type="controls_for" id="IA9dR@+[O_cov8)[Yrpn"><field name="VAR">i</field><value name="FROM"><shadow type="math_number" id="?3X]#_hseoGAUHQkWr6w"><field name="NUM">1</field></shadow></value><value name="TO"><shadow type="math_number" id="`6C.aMWKwE+BP=-P{*-F"><field name="NUM">10</field></shadow><block type="mm_var_return" id="F`+P!,r*?qyvr=yy:|g]"><field name="NAME">numTeeth</field></block></value><value name="BY"><shadow type="math_number" id="/Bd:#K/e8RA5dtPFm/a]"><field name="NUM">1</field></shadow></value><statement name="DO"><block type="mm_code_line" id="Hh{Sj/Sx`d5N;Pzp,rM?"><field name="code">let angle = i*360/numTeeth;</field><next><block type="three_object_add" id="o:)_Alcq_qP~RSg}[4fY"><field name="variable">teeth</field><value name="mesh"><block type="three_rotate" id="sGo6jm#8I0B,f5SKSTG4"><value name="coordinates"><shadow type="mm_xyz" id="@oLg5MaN?RNkmbX90/iB"><field name="X">0</field><field name="Y">0</field><field name="Z">0</field></shadow><block type="mm_list_xyz" id="A7eRrAQ4)z6)2Yo5;Lc["><value name="X"><shadow type="math_number" id="g8N*^MDg{/F,%biR:|a5"><field name="NUM">0</field></shadow></value><value name="Y"><shadow type="math_number" id="A:1yX5|3x(7E=Sa1{WMb"><field name="NUM">0</field></shadow></value><value name="Z"><shadow type="math_number" id="Ax+:=5{TByPHGw2E*^`("><field name="NUM">0</field></shadow><block type="mm_var_return" id="}T954c}Wi;#eP8KbcQ71"><field name="NAME">angle</field></block></value></block></value><value name="object"><shadow type="three_box" id="]G^+yY0/t,ynxjOpGTeJ"><value name="size"><shadow type="mm_xyz" id="09kPa5hiBLx}Bx*.sXs+"><field name="X">100</field><field name="Y">100</field><field name="Z">100</field></shadow></value><value name="segments"><shadow type="math_number" id="cLCanZVA5q7vm#%iD}Ft"><field name="NUM">1</field></shadow></value></shadow><block type="three_clone" id="qXiFDnV6BMXMU_OXe9No"><value name="object"><block type="mm_var_return" id="|rLFr|N;i=jJTOeUXMSj"><field name="NAME">tooth</field></block></value></block></value></block></value></block></next></block></statement><next><block type="mm_var" id="GYR|,Bv5vec+r?G*.ip9"><field name="var">rootDiameter</field><value name="val"><block type="math_arithmetic" id="kE{}Wx80UgSjIu;lObLA"><field name="OP">MULTIPLY</field><value name="A"><shadow type="math_number" id="F`5mYWy.^hjpEmk9O!A="><field name="NUM">1</field></shadow><block type="mm_var_return" id="*ID?navYfhPTWMrc8ZS3"><field name="NAME">rootRadius</field></block></value><value name="B"><shadow type="math_number" id="FL5BO/N?YDK#D=S#P2gn"><field name="NUM">2</field></shadow></value></block></value><next><block type="mm_return" id="I=TkUAwX*UClk6~-L*~q"><value name="ret"><shadow type="math_number" id="gOxE1aT,}o//A3r1/0S|"><field name="NUM">0</field></shadow><block type="three_part" id="-_ek%B4SSsmQ^z7HVP)6"><field name="boolean_op">subtract</field><value name="block1"><shadow type="three_box" id="E=WHWW9q9O=bKFrf^jcU"><value name="size"><shadow type="mm_xyz" id="8CMUcw.SJPiMwrx}2E?n"><field name="X">100</field><field name="Y">100</field><field name="Z">100</field></shadow></value><value name="segments"><shadow type="math_number" id="3OZ8/S}YP%TB5FFlw7f["><field name="NUM">1</field></shadow></value></shadow><block type="three_part" id="T3bs+trq|((|gh-hd|{6"><field name="boolean_op">union</field><value name="block1"><shadow type="three_box" id="1An=hamqud8*ZMc(}aIi"><value name="size"><shadow type="mm_xyz" id="H}L#/DnQJ93xR@:ph=0,"><field name="X">100</field><field name="Y">100</field><field name="Z">100</field></shadow></value><value name="segments"><shadow type="math_number" id="t1Y=}9o/=dM?FTb7}Z-n"><field name="NUM">1</field></shadow></value></shadow><block type="three_translate" id="|wo1-KC#x%%^:S3}}NI?"><value name="coordinates"><shadow type="mm_xyz" id="`iDrk=h?w;aaC_H]:kZ-"><field name="X">0</field><field name="Y">0</field><field name="Z">0</field></shadow><block type="mm_list_xyz" id="{b6(/oPM9.J1+Xx?T#*j"><value name="X"><shadow type="math_number" id="Ai-`X(/4uFVlxLEaQ/H4"><field name="NUM">0</field></shadow></value><value name="Y"><shadow type="math_number" id="xs1dr/N]0WJRb?`]bMOi"><field name="NUM">0</field></shadow></value><value name="Z"><shadow type="math_number" id="`zIlvzvr5OoiMGAZuS:4"><field name="NUM">0</field></shadow><block type="math_arithmetic" id=",{[g^xB81jkKG=^j/1@9"><field name="OP">DIVIDE</field><value name="A"><shadow type="math_number" id="A2(igX`XBFI`V_*}UL@s"><field name="NUM">1</field></shadow><block type="mm_var_return" id="FVJ?5%[-KahC8Bz/7ZEe"><field name="NAME">-thickness</field></block></value><value name="B"><shadow type="math_number" id="cf.n%yDQywp=Fs,6(zmR"><field name="NUM">2</field></shadow></value></block></value></block></value><value name="object"><shadow type="three_box" id="RUd[OD{e4l.cq?7ZVtV_"><value name="size"><shadow type="mm_xyz" id="D#5-NL;WxS5J:[p*TxiU"><field name="X">100</field><field name="Y">100</field><field name="Z">100</field></shadow></value><value name="segments"><shadow type="math_number" id="fq/5^m~s9oj1Jq]z9*[3"><field name="NUM">1</field></shadow></value></shadow><block type="mm_var_return" id="v,olF7SF-1|4JVE,oHrZ"><field name="NAME">teeth</field></block></value></block></value><value name="block2"><shadow type="three_sphere" id="X/bG=Bf3sUIOnzTojx?h"><value name="diameter"><shadow type="math_number" id="Gw=JQM)UFFLW0t]~|wCH"><field name="NUM">120</field></shadow></value><value name="resolution"><shadow type="math_number" id="1C7,!fM~b/tiKlA.NN5|"><field name="NUM">16</field></shadow></value></shadow><block type="three_cylinder" id="0(m7Hiw8Bu]ZS,dw)=}I"><value name="diameter1"><shadow type="math_number" id="p!flb/5AMkqFl5ui@1Jv"><field name="NUM">50</field></shadow><block type="mm_var_return" id="b]%s%t*+1;okOv[|,A++"><field name="NAME">rootDiameter</field></block></value><value name="diameter2"><shadow type="math_number" id="j%*CAUCfMl8YTFKj.hJk"><field name="NUM">50</field></shadow><block type="mm_var_return" id="v%#NbV:T:LA8/SL,Q_e1"><field name="NAME">rootDiameter</field></block></value><value name="height"><shadow type="math_number" id="40CjcGB4ak1zl}fASbF0"><field name="NUM">100</field></shadow><block type="mm_var_return" id="s,]rwDpDT)o!2MAg{9cA"><field name="NAME">thickness</field></block></value><value name="sides"><shadow type="math_number" id="JH-6+T/W_e%/mJ,4;vIa"><field name="NUM">16</field></shadow><block type="math_arithmetic" id=";O?=E.-[kD0rL:OZz/_."><field name="OP">MULTIPLY</field><value name="A"><shadow type="math_number" id="F`5mYWy.^hjpEmk9O!A="><field name="NUM">1</field></shadow><block type="mm_var_return" id="S#@%g**NE!rYv3qU0jZ{"><field name="NAME">numTeeth</field></block></value><value name="B"><shadow type="math_number" id="JyZR)N@y-7Sp|u{WW.n6"><field name="NUM">2</field></shadow></value></block></value></block></value></block></value><value name="block2"><shadow type="three_sphere" id="@0d{NyP0A19i]},I90??"><value name="diameter"><shadow type="math_number" id=")3*E#v6?n3ks_Z:7#/J|"><field name="NUM">120</field></shadow></value><value name="resolution"><shadow type="math_number" id="J04;,3?.YZ7jLM+it*a("><field name="NUM">16</field></shadow></value></shadow><block type="three_translate" id=":-xKjNRxT%[~f#0,57Xe"><value name="coordinates"><shadow type="mm_xyz" id="!r?[Uak8pvml0]UAZBnZ"><field name="X">0</field><field name="Y">0</field><field name="Z">0</field></shadow><block type="mm_list_xyz" id="}dMN5i!r@LL5stL4,h`?"><value name="X"><shadow type="math_number" id="2VmwBwv-`e6q%P`qAZp]"><field name="NUM">0</field></shadow></value><value name="Y"><shadow type="math_number" id="rzuP*=iYbNkVnMt2pUk1"><field name="NUM">0</field></shadow></value><value name="Z"><shadow type="math_number" id="FXzai[4CeCS_sqLu16t="><field name="NUM">-1</field></shadow></value></block></value><value name="object"><shadow type="three_box" id="8Rs?@cFxZK,7V}{k6r?d"><value name="size"><shadow type="mm_xyz" id="*fT}w|~W~,J_WiQA]!gg"><field name="X">100</field><field name="Y">100</field><field name="Z">100</field></shadow></value><value name="segments"><shadow type="math_number" id="NOR|Y|i2-q_Fthh{cihU"><field name="NUM">1</field></shadow></value></shadow><block type="three_cylinder" id="=.sg*C@A}VjnT!RLL2@S"><value name="diameter1"><shadow type="math_number" id="N95p~d[YpaFwP4sEnLHa"><field name="NUM">50</field></shadow><block type="mm_var_return" id="3kC;_3b5SCN5#D84QuHk"><field name="NAME">hole</field></block></value><value name="diameter2"><shadow type="math_number" id="T%Z^XyP@/uYFmwKUz,@e"><field name="NUM">50</field></shadow><block type="mm_var_return" id="q!kPHuTf}Nw2tH#BY01i"><field name="NAME">hole</field></block></value><value name="height"><shadow type="math_number" id="5K](!FBNm,_P@~JyA8Nw"><field name="NUM">100</field></shadow><block type="math_arithmetic" id="EBz#EM#lNMIA3Br8[]os"><field name="OP">ADD</field><value name="A"><shadow type="math_number" id="F`5mYWy.^hjpEmk9O!A="><field name="NUM">1</field></shadow><block type="mm_var_return" id="sq;L=gzJxdv2A3-(a`WQ"><field name="NAME">thickness</field></block></value><value name="B"><shadow type="math_number" id="x(.r2g567QfY@iE!/W=*"><field name="NUM">2</field></shadow></value></block></value><value name="sides"><shadow type="math_number" id="f2+!kT)i{9Fu|N`Va)K2"><field name="NUM">32</field></shadow></value></block></value></block></value></block></value></block></next></block></next></block></next></block></next></block></next></block></statement><next><block type="three_scene_add" id="feIvS*E(}8z!U0|hi_kl"><value name="mesh"><shadow type="three_box" id="^O^|Ui%WyqyJc`d@={uF"><value name="size"><shadow type="mm_xyz" id="5jn2zh=V^Js:Z_P4rFPU"><field name="X">100</field><field name="Y">100</field><field name="Z">100</field></shadow></value><value name="segments"><shadow type="math_number" id="RJ18kx4|%bXRG|@/d!8R"><field name="NUM">1</field></shadow></value></shadow><block type="bi_call_editable_return" id="9SykW*X05O=81fOLO@-j"><mutation items="6"></mutation><field name="NAME">involuteGear</field><value name="items1"><block type="math_number" id="x3z5%!C=~V9~6jxOPLvk"><field name="NUM">15</field></block></value><value name="items2"><block type="math_number" id="WD[^(5Kvw-XRj8sWm/@n"><field name="NUM">10</field></block></value><value name="items3"><block type="math_number" id="UA7uSy)T}[76-RM(w%.y"><field name="NUM">20</field></block></value><value name="items4"><block type="math_number" id="yE|GJGyNVkD7KUR)Zoab"><field name="NUM">10</field></block></value><value name="items5"><block type="math_number" id="5m+2a2Y(Fndp?3w@d[Qs"><field name="NUM">15</field></block></value></block></value></block></next></block></next></block></xml>PK
     �{�J�1��7  7     jsvar i;


mm_scene = null;
mm_scene = new THREE.Scene();
mm_scene.add(new THREE.HemisphereLight('#ffffff', '#666666', 1));
function involuteGear(numTeeth, circularPitch, thickness, hole, twist, pressureAngle, clearance){
  var teeth = mm_set(new THREE.Object3D());
  // Parametric gear derivate from:
  // http://www.thingiverse.com/thing:17584
  // https://github.com/carvadero/OpenJsCsg/blob/gh-pages/src/csg.js

  function add2D(p1, p2){
      return [p1[0]+p2[0], p1[1]+p2[1]];
  }

  function times2D(p, c){
      return [p[0]*c, p[1]*c];
  }

  function normal2D(p){
      return [p[1], -p[0]];
  }

  /*
    For gear terminology see:
      http://www.astronomiainumbria.org/advanced_internet_files/meccanica/easyweb.easynet.co.uk/_chrish/geardata.htm
    Algorithm based on:
      http://www.cartertools.com/involute.html

    circularPitch: The distance between adjacent teeth measured at the pitch circle
  */

  // default values:
  if(arguments.length < 3) thickness = 10;
  if(arguments.length < 4) hole = 0;
  if(arguments.length < 5) twist = 0;
  if(arguments.length < 6) pressureAngle = 20;
  if(arguments.length < 7) clearance = 0;

  var addendum = circularPitch / Math.PI;
  var dedendum = addendum + clearance;

  // radiuses of the 4 circles:
  var pitchRadius = numTeeth * circularPitch / (2 * Math.PI);
  var baseRadius = pitchRadius * Math.cos(Math.PI * pressureAngle / 180);
  var outerRadius = pitchRadius + addendum;
  var rootRadius = pitchRadius - dedendum;

  var maxtanlength = Math.sqrt(outerRadius*outerRadius - baseRadius*baseRadius);
  var maxangle = maxtanlength / baseRadius;

  var tl_at_pitchcircle = Math.sqrt(pitchRadius*pitchRadius - baseRadius*baseRadius);
  var angle_at_pitchcircle = tl_at_pitchcircle / baseRadius;
  var diffangle = angle_at_pitchcircle - Math.atan(angle_at_pitchcircle);
  var angularToothWidthAtBase = Math.PI / numTeeth + 2*diffangle;

  // build a single 2d tooth in the 'points' array:
  var resolution = 5;
  var points = [[0,0]]; // var points = [new CSG.Vector2D(0,0)];
  for(var i = 0; i <= resolution; i++)
  {
      // first side of the tooth:
      var angle = maxangle * i / resolution;
      var tanlength = angle * baseRadius;
      var radvector = [Math.cos(angle), Math.sin(angle)];  // CSG.Vector2D.fromAngle(angle);
      var tanvector = normal2D(radvector); // radvector.normal(); // Rotate 90º Clockwise
      var p = add2D(times2D(radvector, baseRadius), times2D(tanvector, tanlength));  // radvector.times(baseRadius).plus(tanvector.times(tanlength));
      points[i+1] = p;
      console.log(p);
      
      // opposite side of the tooth:
      var angleOpposite = angularToothWidthAtBase - angle;
      radvector = [Math.cos(angleOpposite), Math.sin(angleOpposite)]; // CSG.Vector2D.fromAngle(angularToothWidthAtBase - angle);
      tanvector = times2D(normal2D(radvector),-1); // radvector.normal().negated();
      p = add2D(times2D(radvector, baseRadius), times2D(tanvector, tanlength));  // radvector.times(baseRadius).plus(tanvector.times(tanlength));
      points[2 * resolution + 2 - i] = p;
      console.log(p);
  }


  var toothShape = mm_polygon(points);


  var tooth = mm_extrude(toothShape, thickness, 10, twist);
  var i_inc = 1;
  if (1 > numTeeth) {
    i_inc = -i_inc;
  }
  for (i = 1;
       i_inc >= 0 ? i <= numTeeth : i >= numTeeth;
       i += i_inc) {
    let angle = i*360/numTeeth;
    teeth.add((tooth.clone())
      .setRotation([0, 0, angle]));
  }
  var rootDiameter = (rootRadius * 2);
  return mm_boolean_op('subtract', mm_boolean_op('union', teeth
    .setTranslation([0, 0, (-thickness / 2)]), mm_cylinder(rootDiameter, rootDiameter, thickness, (numTeeth * 2), 1)), mm_cylinder(hole, hole, (thickness + 2), 32, 1)
    .setTranslation([0, 0, -1]))
}
mm_scene.add(involuteGear(15, 10, 20, 10, 15));
PK
     �{�J����    	   functions{"script1.js":{"hash":"0_d41d8cd98f00b204e9800998ecf8427e","name":"script1.js","data":"// Parametric gear derivate from:\r\n// http://www.thingiverse.com/thing:17584\r\n// https://github.com/carvadero/OpenJsCsg/blob/gh-pages/src/csg.js\r\n\r\nfunction add2D(p1, p2){\r\n    return [p1[0]+p2[0], p1[1]+p2[1]];\r\n}\r\n\r\nfunction times2D(p, c){\r\n    return [p[0]*c, p[1]*c];\r\n}\r\n\r\nfunction normal2D(p){\r\n    return [p[1], -p[0]];\r\n}\r\n\r\n/*\r\n  For gear terminology see:\r\n    http://www.astronomiainumbria.org/advanced_internet_files/meccanica/easyweb.easynet.co.uk/_chrish/geardata.htm\r\n  Algorithm based on:\r\n    http://www.cartertools.com/involute.html\r\n\r\n  circularPitch: The distance between adjacent teeth measured at the pitch circle\r\n*/\r\n\r\n// default values:\r\nif(arguments.length < 3) thickness = 10;\r\nif(arguments.length < 4) hole = 0;\r\nif(arguments.length < 5) twist = 0;\r\nif(arguments.length < 6) pressureAngle = 20;\r\nif(arguments.length < 7) clearance = 0;\r\n\r\nvar addendum = circularPitch / Math.PI;\r\nvar dedendum = addendum + clearance;\r\n\r\n// radiuses of the 4 circles:\r\nvar pitchRadius = numTeeth * circularPitch / (2 * Math.PI);\r\nvar baseRadius = pitchRadius * Math.cos(Math.PI * pressureAngle / 180);\r\nvar outerRadius = pitchRadius + addendum;\r\nvar rootRadius = pitchRadius - dedendum;\r\n\r\nvar maxtanlength = Math.sqrt(outerRadius*outerRadius - baseRadius*baseRadius);\r\nvar maxangle = maxtanlength / baseRadius;\r\n\r\nvar tl_at_pitchcircle = Math.sqrt(pitchRadius*pitchRadius - baseRadius*baseRadius);\r\nvar angle_at_pitchcircle = tl_at_pitchcircle / baseRadius;\r\nvar diffangle = angle_at_pitchcircle - Math.atan(angle_at_pitchcircle);\r\nvar angularToothWidthAtBase = Math.PI / numTeeth + 2*diffangle;\r\n\r\n// build a single 2d tooth in the 'points' array:\r\nvar resolution = 5;\r\nvar points = [[0,0]]; // var points = [new CSG.Vector2D(0,0)];\r\nfor(var i = 0; i <= resolution; i++)\r\n{\r\n    // first side of the tooth:\r\n    var angle = maxangle * i / resolution;\r\n    var tanlength = angle * baseRadius;\r\n    var radvector = [Math.cos(angle), Math.sin(angle)];  // CSG.Vector2D.fromAngle(angle);\r\n    var tanvector = normal2D(radvector); // radvector.normal(); // Rotate 90º Clockwise\r\n    var p = add2D(times2D(radvector, baseRadius), times2D(tanvector, tanlength));  // radvector.times(baseRadius).plus(tanvector.times(tanlength));\r\n    points[i+1] = p;\r\n    console.log(p);\r\n    \r\n    // opposite side of the tooth:\r\n    var angleOpposite = angularToothWidthAtBase - angle;\r\n    radvector = [Math.cos(angleOpposite), Math.sin(angleOpposite)]; // CSG.Vector2D.fromAngle(angularToothWidthAtBase - angle);\r\n    tanvector = times2D(normal2D(radvector),-1); // radvector.normal().negated();\r\n    p = add2D(times2D(radvector, baseRadius), times2D(tanvector, tanlength));  // radvector.times(baseRadius).plus(tanvector.times(tanlength));\r\n    points[2 * resolution + 2 - i] = p;\r\n    console.log(p);\r\n}\r\n\r\n\r\nvar toothShape = mm_polygon(points);\r\n\r\n","url":""}}PK
     �{�J            	   textures/PK
     �{�JC���         textures/metadata.json{}PK
     �{�J               objects/PK
     �{�JC���         objects/metadata.json{}PK 
     �{�J�dv	   	                    mmpPK 
     �{�J�F�@20  20               *   xmlPK 
     �{�J�1��7  7               }0  jsPK 
     �{�J����    	             �?  functionsPK 
     �{�J            	            L  textures/PK 
     �{�JC���                   7L  textures/metadata.jsonPK 
     �{�J                        mL  objects/PK 
     �{�JC���                   �L  objects/metadata.jsonPK      �  �L    