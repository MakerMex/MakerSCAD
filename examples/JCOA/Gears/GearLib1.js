var i;


function involuteGear(numTeeth, circularPitch, thickness, hole, twist, pressureAngle, clearance){
  var teeth = mm_set(new THREE.Object3D());
  // Parametric gear derivate from:
  // http://www.thingiverse.com/thing:17584
  // https://github.com/carvadero/OpenJsCsg/blob/gh-pages/src/csg.js

  function add2D(p1, p2){
      return [p1[0]+p2[0], p1[1]+p2[1]];
  }

  function times2D(p, c){
      return [p[0]*c, p[1]*c];
  }

  function normal2D(p){
      return [p[1], -p[0]];
  }

  /*
    For gear terminology see:
      http://www.astronomiainumbria.org/advanced_internet_files/meccanica/easyweb.easynet.co.uk/_chrish/geardata.htm
    Algorithm based on:
      http://www.cartertools.com/involute.html

    circularPitch: The distance between adjacent teeth measured at the pitch circle
  */

  // default values:
  if(arguments.length < 3) thickness = 10;
  if(arguments.length < 4) hole = 0;
  if(arguments.length < 5) twist = 0;
  if(arguments.length < 6) pressureAngle = 20;
  if(arguments.length < 7) clearance = 0;

  var addendum = circularPitch / Math.PI;
  var dedendum = addendum + clearance;

  // radiuses of the 4 circles:
  var pitchRadius = numTeeth * circularPitch / (2 * Math.PI);
  var baseRadius = pitchRadius * Math.cos(Math.PI * pressureAngle / 180);
  var outerRadius = pitchRadius + addendum;
  var rootRadius = pitchRadius - dedendum;

  var maxtanlength = Math.sqrt(outerRadius*outerRadius - baseRadius*baseRadius);
  var maxangle = maxtanlength / baseRadius;

  var tl_at_pitchcircle = Math.sqrt(pitchRadius*pitchRadius - baseRadius*baseRadius);
  var angle_at_pitchcircle = tl_at_pitchcircle / baseRadius;
  var diffangle = angle_at_pitchcircle - Math.atan(angle_at_pitchcircle);
  var angularToothWidthAtBase = Math.PI / numTeeth + 2*diffangle;

  // build a single 2d tooth in the 'points' array:
  var resolution = 5;
  var points = [[0,0]]; // var points = [new CSG.Vector2D(0,0)];
  for(var i = 0; i <= resolution; i++)
  {
      // first side of the tooth:
      var angle = maxangle * i / resolution;
      var tanlength = angle * baseRadius;
      var radvector = [Math.cos(angle), Math.sin(angle)];  // CSG.Vector2D.fromAngle(angle);
      var tanvector = normal2D(radvector); // radvector.normal(); // Rotate 90º Clockwise
      var p = add2D(times2D(radvector, baseRadius), times2D(tanvector, tanlength));  // radvector.times(baseRadius).plus(tanvector.times(tanlength));
      points[i+1] = p;
      console.log(p);
      
      // opposite side of the tooth:
      var angleOpposite = angularToothWidthAtBase - angle;
      radvector = [Math.cos(angleOpposite), Math.sin(angleOpposite)]; // CSG.Vector2D.fromAngle(angularToothWidthAtBase - angle);
      tanvector = times2D(normal2D(radvector),-1); // radvector.normal().negated();
      p = add2D(times2D(radvector, baseRadius), times2D(tanvector, tanlength));  // radvector.times(baseRadius).plus(tanvector.times(tanlength));
      points[2 * resolution + 2 - i] = p;
      console.log(p);
  }


  var toothShape = mm_polygon(points);


  var tooth = mm_extrude(toothShape, thickness, 4, 0);
  var i_inc = 1;
  if (1 > numTeeth) {
    i_inc = -i_inc;
  }
  for (i = 1;
       i_inc >= 0 ? i <= numTeeth : i >= numTeeth;
       i += i_inc) {
    let angle = i*360/numTeeth;
    teeth.add((tooth.clone())
      .setRotation([0, 0, angle]));
  }
  var rootDiameter = (rootRadius * 2);
  var gear = mm_boolean_op('subtract', mm_boolean_op('union', teeth
    .setTranslation([0, 0, (-thickness / 2)]), mm_cylinder(rootDiameter, rootDiameter, thickness, (numTeeth * 2), 1)), mm_cylinder(hole, hole, (thickness + 2), 32, 1)
    .setTranslation([0, 0, -1]));
  return gear
}
function involuteGearDouble(numTeeth, circularPitch, thickness, hole, twist, pressureAngle, clearance){
  if(arguments.length < 3) thickness = 10;
  if(arguments.length < 4) hole = 0;
  if(arguments.length < 5) twist = 0;
  if(arguments.length < 6) pressureAngle = 20;
  if(arguments.length < 7) clearance = 0;
  var gear = involuteGear(numTeeth, circularPitch, thickness, hole, twist, pressureAngle, clearance);

  mm_twist_fn(gear.geometry,
    function(z){
      let ret;
      if(z<0){
          ret = z*twist/(thickness*360.0);
      } else {
          ret = -z*twist/(thickness*360.0);
      }
      return ret;
    });
  return gear
}
