PK
     hz�J�dv	   	      mmp{"ui":{}}PK
     hz�Ji��  �     xml<xml xmlns="http://www.w3.org/1999/xhtml"><block type="three_init" id="KO~TW,u_S:w+e06Z`tji" x="-151" y="-308"><field name="lights">TRUE</field><field name="physijs">FALSE</field><next><block type="mm_script_text" id="Mzs`2+cN5vekY7ff*{iA"><field name="name">involuteGear.js</field><next><block type="three_scene_add" id="feIvS*E(}8z!U0|hi_kl"><value name="mesh"><shadow type="three_box" id="^O^|Ui%WyqyJc`d@={uF"><value name="size"><shadow type="mm_xyz" id="5jn2zh=V^Js:Z_P4rFPU"><field name="X">100</field><field name="Y">100</field><field name="Z">100</field></shadow></value><value name="segments"><shadow type="math_number" id="RJ18kx4|%bXRG|@/d!8R"><field name="NUM">1</field></shadow></value></shadow><block type="bi_call_editable_return" id="9SykW*X05O=81fOLO@-j"><mutation items="5"></mutation><field name="NAME">involuteGear</field><value name="items1"><block type="math_number" id="x3z5%!C=~V9~6jxOPLvk"><field name="NUM">15</field></block></value><value name="items2"><block type="math_number" id="WD[^(5Kvw-XRj8sWm/@n"><field name="NUM">10</field></block></value><value name="items3"><block type="math_number" id="UA7uSy)T}[76-RM(w%.y"><field name="NUM">20</field></block></value><value name="items4"><block type="math_number" id="yE|GJGyNVkD7KUR)Zoab"><field name="NUM">15</field></block></value></block></value><next><block type="three_scene_add" id="YxWJChh29vRHtIC*1gL%"><value name="mesh"><shadow type="three_box" id="^O^|Ui%WyqyJc`d@={uF"><value name="size"><shadow type="mm_xyz" id="5jn2zh=V^Js:Z_P4rFPU"><field name="X">100</field><field name="Y">100</field><field name="Z">100</field></shadow></value><value name="segments"><shadow type="math_number" id="RJ18kx4|%bXRG|@/d!8R"><field name="NUM">1</field></shadow></value></shadow><block type="three_translate" id="K.|X[+GDwT~}TRyaAee}"><value name="coordinates"><shadow type="mm_xyz" id="C-VF+f%iyNIX8y(=!1yJ"><field name="X">50</field><field name="Y">0</field><field name="Z">0</field></shadow></value><value name="object"><shadow type="three_box" id="4(jMyWMzDaRRqSRe|M3:"><value name="size"><shadow type="mm_xyz" id="`Xsp2-jlFKu}XW7VHZGP"><field name="X">100</field><field name="Y">100</field><field name="Z">100</field></shadow></value><value name="segments"><shadow type="math_number" id="fC!_[=s[6TpMNTS[x=y|"><field name="NUM">1</field></shadow></value></shadow><block type="bi_call_editable_return" id="6+(??Z{H8EvtpNZUtq9~"><mutation items="5"></mutation><field name="NAME">involuteGear</field><value name="items1"><block type="math_number" id="UNWP:%:_!.n4q0:JnK~m"><field name="NUM">8</field></block></value><value name="items2"><block type="math_number" id="5{jQOEslp`ka`3j[E`g4"><field name="NUM">10</field></block></value><value name="items3"><block type="math_number" id=",Au3pzGyCEOSe8]-#;GR"><field name="NUM">20</field></block></value><value name="items4"><block type="math_number" id="e_./y@qC%CN6AS?mJ~x8"><field name="NUM">-15</field></block></value></block></value></block></value></block></next></block></next></block></next></block></xml>PK
     hz�J���  �     jsmm_scene = null;
mm_scene = new THREE.Scene();
mm_scene.add(new THREE.HemisphereLight('#ffffff', '#666666', 1));
var i;


function involuteGear(numTeeth, circularPitch, thickness, twist, pressureAngle, clearance){
  var teeth = mm_set(new THREE.Object3D());
  // Parametric gear derivate from:
  // http://www.thingiverse.com/thing:17584
  // https://github.com/carvadero/OpenJsCsg/blob/gh-pages/src/csg.js

  function add2D(p1, p2){
      return [p1[0]+p2[0], p1[1]+p2[1]];
  }

  function times2D(p, c){
      return [p[0]*c, p[1]*c];
  }

  function normal2D(p){
      return [p[1], -p[0]];
  }

  /*
    For gear terminology see:
      http://www.astronomiainumbria.org/advanced_internet_files/meccanica/easyweb.easynet.co.uk/_chrish/geardata.htm
    Algorithm based on:
      http://www.cartertools.com/involute.html

    circularPitch: The distance between adjacent teeth measured at the pitch circle
  */

  // default values:
  if(arguments.length < 3) thickness = 10;
  if(arguments.length < 4) twist = 0;
  if(arguments.length < 5) pressureAngle = 20;
  if(arguments.length < 6) clearance = 0;

  var addendum = circularPitch / Math.PI;
  var dedendum = addendum + clearance;

  // radiuses of the 4 circles:
  var pitchRadius = numTeeth * circularPitch / (2 * Math.PI);
  var baseRadius = pitchRadius * Math.cos(Math.PI * pressureAngle / 180);
  var outerRadius = pitchRadius + addendum;
  var rootRadius = pitchRadius - dedendum;

  var maxtanlength = Math.sqrt(outerRadius*outerRadius - baseRadius*baseRadius);
  var maxangle = maxtanlength / baseRadius;

  var tl_at_pitchcircle = Math.sqrt(pitchRadius*pitchRadius - baseRadius*baseRadius);
  var angle_at_pitchcircle = tl_at_pitchcircle / baseRadius;
  var diffangle = angle_at_pitchcircle - Math.atan(angle_at_pitchcircle);
  var angularToothWidthAtBase = Math.PI / numTeeth + 2*diffangle;

  // build a single 2d tooth in the 'points' array:
  var resolution = 5;
  var points = [[0,0]]; // var points = [new CSG.Vector2D(0,0)];
  for(var i = 0; i <= resolution; i++)
  {
      // first side of the tooth:
      var angle = maxangle * i / resolution;
      var tanlength = angle * baseRadius;
      var radvector = [Math.cos(angle), Math.sin(angle)];  // CSG.Vector2D.fromAngle(angle);
      var tanvector = normal2D(radvector); // radvector.normal(); // Rotate 90º Clockwise
      var p = add2D(times2D(radvector, baseRadius), times2D(tanvector, tanlength));  // radvector.times(baseRadius).plus(tanvector.times(tanlength));
      points[i+1] = p;
      console.log(p);

      // opposite side of the tooth:
      var angleOpposite = angularToothWidthAtBase - angle;
      radvector = [Math.cos(angleOpposite), Math.sin(angleOpposite)]; // CSG.Vector2D.fromAngle(angularToothWidthAtBase - angle);
      tanvector = times2D(normal2D(radvector),-1); // radvector.normal().negated();
      p = add2D(times2D(radvector, baseRadius), times2D(tanvector, tanlength));  // radvector.times(baseRadius).plus(tanvector.times(tanlength));
      points[2 * resolution + 2 - i] = p;
      console.log(p);
  }


  var toothShape = mm_polygon(points);


  var tooth = mm_extrude(toothShape, thickness, 10, twist);
  var i_inc = 1;
  if (1 > numTeeth) {
    i_inc = -i_inc;
  }
  for (i = 1;
       i_inc >= 0 ? i <= numTeeth : i >= numTeeth;
       i += i_inc) {
    let angle = i*360/numTeeth;
    teeth.add((tooth.clone())
      .setRotation([0, 0, angle]));
  }
  var rootDiameter = (rootRadius * 2);
  return mm_boolean_op('union', teeth
    .setTranslation([0, 0, (-thickness / 2)]), mm_cylinder(rootDiameter, rootDiameter, thickness, (numTeeth * 2), 1))
}

mm_scene.add(involuteGear(15, 10, 20, 15));
mm_scene.add(involuteGear(8, 10, 20, -15)
  .setTranslation([50, 0, 0]));
PK
     hz�J�	��s  s  	   functions{"script1.js":{"hash":"af7_6d1c007a24accc1f480788165b202b52","name":"script1.js","data":"// Parametric gear derivate from:\r\n// http://www.thingiverse.com/thing:17584\r\n// https://github.com/carvadero/OpenJsCsg/blob/gh-pages/src/csg.js\r\n\r\nfunction add2D(p1, p2){\r\n    return [p1[0]+p2[0], p1[1]+p2[1]];\r\n}\r\n\r\nfunction times2D(p, c){\r\n    return [p[0]*c, p[1]*c];\r\n}\r\n\r\nfunction normal2D(p){\r\n    return [p[1], -p[0]];\r\n}\r\n\r\n/*\r\n  For gear terminology see:\r\n    http://www.astronomiainumbria.org/advanced_internet_files/meccanica/easyweb.easynet.co.uk/_chrish/geardata.htm\r\n  Algorithm based on:\r\n    http://www.cartertools.com/involute.html\r\n\r\n  circularPitch: The distance between adjacent teeth measured at the pitch circle\r\n*/\r\n\r\n// default values:\r\nif(arguments.length < 3) thickness = 10;\r\nif(arguments.length < 4) twist = 0;\r\nif(arguments.length < 5) pressureAngle = 20;\r\nif(arguments.length < 6) clearance = 0;\r\n\r\nvar addendum = circularPitch / Math.PI;\r\nvar dedendum = addendum + clearance;\r\n\r\n// radiuses of the 4 circles:\r\nvar pitchRadius = numTeeth * circularPitch / (2 * Math.PI);\r\nvar baseRadius = pitchRadius * Math.cos(Math.PI * pressureAngle / 180);\r\nvar outerRadius = pitchRadius + addendum;\r\nvar rootRadius = pitchRadius - dedendum;\r\n\r\nvar maxtanlength = Math.sqrt(outerRadius*outerRadius - baseRadius*baseRadius);\r\nvar maxangle = maxtanlength / baseRadius;\r\n\r\nvar tl_at_pitchcircle = Math.sqrt(pitchRadius*pitchRadius - baseRadius*baseRadius);\r\nvar angle_at_pitchcircle = tl_at_pitchcircle / baseRadius;\r\nvar diffangle = angle_at_pitchcircle - Math.atan(angle_at_pitchcircle);\r\nvar angularToothWidthAtBase = Math.PI / numTeeth + 2*diffangle;\r\n\r\n// build a single 2d tooth in the 'points' array:\r\nvar resolution = 5;\r\nvar points = [[0,0]]; // var points = [new CSG.Vector2D(0,0)];\r\nfor(var i = 0; i <= resolution; i++)\r\n{\r\n    // first side of the tooth:\r\n    var angle = maxangle * i / resolution;\r\n    var tanlength = angle * baseRadius;\r\n    var radvector = [Math.cos(angle), Math.sin(angle)];  // CSG.Vector2D.fromAngle(angle);\r\n    var tanvector = normal2D(radvector); // radvector.normal(); // Rotate 90º Clockwise\r\n    var p = add2D(times2D(radvector, baseRadius), times2D(tanvector, tanlength));  // radvector.times(baseRadius).plus(tanvector.times(tanlength));\r\n    points[i+1] = p;\r\n    console.log(p);\r\n    \r\n    // opposite side of the tooth:\r\n    var angleOpposite = angularToothWidthAtBase - angle;\r\n    radvector = [Math.cos(angleOpposite), Math.sin(angleOpposite)]; // CSG.Vector2D.fromAngle(angularToothWidthAtBase - angle);\r\n    tanvector = times2D(normal2D(radvector),-1); // radvector.normal().negated();\r\n    p = add2D(times2D(radvector, baseRadius), times2D(tanvector, tanlength));  // radvector.times(baseRadius).plus(tanvector.times(tanlength));\r\n    points[2 * resolution + 2 - i] = p;\r\n    console.log(p);\r\n}\r\n\r\n\r\nvar toothShape = mm_polygon(points);\r\n\r\n","url":""},"involuteGear.js":{"hash":"dfd_2d9c3efd3fea7013529a9ca35f3520de","name":"involuteGear.js","data":"var i;\n\n\nfunction involuteGear(numTeeth, circularPitch, thickness, twist, pressureAngle, clearance){\n  var teeth = mm_set(new THREE.Object3D());\n  // Parametric gear derivate from:\n  // http://www.thingiverse.com/thing:17584\n  // https://github.com/carvadero/OpenJsCsg/blob/gh-pages/src/csg.js\n\n  function add2D(p1, p2){\n      return [p1[0]+p2[0], p1[1]+p2[1]];\n  }\n\n  function times2D(p, c){\n      return [p[0]*c, p[1]*c];\n  }\n\n  function normal2D(p){\n      return [p[1], -p[0]];\n  }\n\n  /*\n    For gear terminology see:\n      http://www.astronomiainumbria.org/advanced_internet_files/meccanica/easyweb.easynet.co.uk/_chrish/geardata.htm\n    Algorithm based on:\n      http://www.cartertools.com/involute.html\n\n    circularPitch: The distance between adjacent teeth measured at the pitch circle\n  */\n\n  // default values:\n  if(arguments.length < 3) thickness = 10;\n  if(arguments.length < 4) twist = 0;\n  if(arguments.length < 5) pressureAngle = 20;\n  if(arguments.length < 6) clearance = 0;\n\n  var addendum = circularPitch / Math.PI;\n  var dedendum = addendum + clearance;\n\n  // radiuses of the 4 circles:\n  var pitchRadius = numTeeth * circularPitch / (2 * Math.PI);\n  var baseRadius = pitchRadius * Math.cos(Math.PI * pressureAngle / 180);\n  var outerRadius = pitchRadius + addendum;\n  var rootRadius = pitchRadius - dedendum;\n\n  var maxtanlength = Math.sqrt(outerRadius*outerRadius - baseRadius*baseRadius);\n  var maxangle = maxtanlength / baseRadius;\n\n  var tl_at_pitchcircle = Math.sqrt(pitchRadius*pitchRadius - baseRadius*baseRadius);\n  var angle_at_pitchcircle = tl_at_pitchcircle / baseRadius;\n  var diffangle = angle_at_pitchcircle - Math.atan(angle_at_pitchcircle);\n  var angularToothWidthAtBase = Math.PI / numTeeth + 2*diffangle;\n\n  // build a single 2d tooth in the 'points' array:\n  var resolution = 5;\n  var points = [[0,0]]; // var points = [new CSG.Vector2D(0,0)];\n  for(var i = 0; i <= resolution; i++)\n  {\n      // first side of the tooth:\n      var angle = maxangle * i / resolution;\n      var tanlength = angle * baseRadius;\n      var radvector = [Math.cos(angle), Math.sin(angle)];  // CSG.Vector2D.fromAngle(angle);\n      var tanvector = normal2D(radvector); // radvector.normal(); // Rotate 90º Clockwise\n      var p = add2D(times2D(radvector, baseRadius), times2D(tanvector, tanlength));  // radvector.times(baseRadius).plus(tanvector.times(tanlength));\n      points[i+1] = p;\n      console.log(p);\n      \n      // opposite side of the tooth:\n      var angleOpposite = angularToothWidthAtBase - angle;\n      radvector = [Math.cos(angleOpposite), Math.sin(angleOpposite)]; // CSG.Vector2D.fromAngle(angularToothWidthAtBase - angle);\n      tanvector = times2D(normal2D(radvector),-1); // radvector.normal().negated();\n      p = add2D(times2D(radvector, baseRadius), times2D(tanvector, tanlength));  // radvector.times(baseRadius).plus(tanvector.times(tanlength));\n      points[2 * resolution + 2 - i] = p;\n      console.log(p);\n  }\n\n\n  var toothShape = mm_polygon(points);\n\n\n  var tooth = mm_extrude(toothShape, thickness, 10, twist);\n  var i_inc = 1;\n  if (1 > numTeeth) {\n    i_inc = -i_inc;\n  }\n  for (i = 1;\n       i_inc >= 0 ? i <= numTeeth : i >= numTeeth;\n       i += i_inc) {\n    let angle = i*360/numTeeth;\n    teeth.add((tooth.clone())\n      .setRotation([0, 0, angle]));\n  }\n  var rootDiameter = (rootRadius * 2);\n  return mm_boolean_op('union', teeth\n    .setTranslation([0, 0, (-thickness / 2)]), mm_cylinder(rootDiameter, rootDiameter, thickness, (numTeeth * 2), 1))\n}\n","url":""}}PK
     hz�J            	   textures/PK
     hz�JC���         textures/metadata.json{}PK
     hz�J               objects/PK
     hz�JC���         objects/metadata.json{}PK 
     hz�J�dv	   	                    mmpPK 
     hz�Ji��  �               *   xmlPK 
     hz�J���  �               %  jsPK 
     hz�J�	��s  s  	             �  functionsPK 
     hz�J            	            u5  textures/PK 
     hz�JC���                   �5  textures/metadata.jsonPK 
     hz�J                        �5  objects/PK 
     hz�JC���                   �5  objects/metadata.jsonPK      �  -6    