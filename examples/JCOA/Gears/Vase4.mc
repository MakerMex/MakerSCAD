PK
     ��J�dv	   	      mmp{"ui":{}}PK
     ��J��f�r  r     xml<xml xmlns="http://www.w3.org/1999/xhtml"><block type="three_init" id="mwYh?BBA3#yQs**}%E*d" x="-233" y="14"><field name="lights">TRUE</field><field name="physijs">FALSE</field><next><block type="bi_named_function" id="P9xfdB4p;g=99K`/!ck]"><field name="function_type">function </field><field name="name">print_points</field><field name="args">polygon</field><statement name="chain"><block type="mm_script_text" id="~agW1EAGzCWZJQ{TaxE["><field name="name">print_points.js</field></block></statement><next><block type="bi_named_function" id="gNo-;At31OE2*#2/E|UV"><field name="function_type">function </field><field name="name">z_fn</field><field name="args">shape, z_n, z_min, z_max</field><statement name="chain"><block type="mm_script_text" id="Wv%iyCA?oWMCF=y8EHAQ"><field name="name">z_fn.js</field></block></statement><next><block type="mm_script_text" id="Z8X`H.0NVn`CdMTp9,Be"><field name="name">GearLib2.js</field><next><block type="mm_var" id="rFVYh:X:Y263HP}o/[Y5"><field name="var">f_z</field><value name="val"><block type="bi_call_editable_return" id="+fe,%l6ek,z%lr?]vLLt"><mutation items="5"></mutation><field name="NAME">z_fn</field><value name="items1"><block type="three_polygon" id="!sF8`X,|[1%)V+_ID~9l"><field name="code">[[0,0],[8,8],[1,16],[9,19],[12,24],[7,27],[13,33]]</field></block></value><value name="items2"><block type="math_number" id=".P?JL!5:2QpYSO51O9%#"><field name="NUM">200</field></block></value><value name="items3"><block type="math_number" id="cqo]*N21+uNCg2Xw?XGu"><field name="NUM">0</field></block></value><value name="items4"><block type="math_number" id="6F@B!]Tu-E#.;SzpUeIJ"><field name="NUM">1.01</field></block></value></block></value><next><block type="mm_println" id="ufulN}M^Z){)+#+HG3Py"><value name="msg"><shadow type="text" id="zuSOgU^mJ[g/HEeiOHxI"><field name="TEXT">____</field></shadow></value><next><block type="three_scene_add" id="AF7qHkQ?~n/GQ?J?P:s-"><value name="mesh"><shadow type="three_box" id="!=_sr@{Q%u2aKdiy[f!:"><value name="size"><shadow type="mm_xyz" id="_Lab|T0XJ?Cm=R-d*yEs"><field name="X">100</field><field name="Y">100</field><field name="Z">100</field></shadow></value><value name="segments"><shadow type="math_number" id="o:TSK;VBm|Yp6co5v*fa"><field name="NUM">1</field></shadow></value></shadow><block type="three_extrude_spiral" id="Q3}{z|^4HZ|J@4q(gOx*"><value name="height"><shadow type="math_number" id="6DH[=oS}Q/Lf@@=)1-0Z"><field name="NUM">80</field></shadow></value><value name="sections"><shadow type="math_number" id="1iz!tZf|x7lG6P[f~?pg"><field name="NUM">400</field></shadow></value><value name="polygon"><shadow type="three_polygon" id="imewXj^q^OeKI^cJHh+h"><field name="code">[[0,0],[10,0],[0,10]]</field></shadow></value><statement name="rotations_p"><shadow type="mm_return" id="#cCmuKMd3RP-r3j0jvhO"><value name="ret"><shadow type="math_arithmetic" id="Zt{#21W,0xP}DdT4*#}}"><field name="OP">MULTIPLY</field><value name="A"><shadow type="mm_var_return" id="tehRCbkmUt7E9!LtAA,q"><field name="NAME">p</field></shadow></value><value name="B"><shadow type="math_number" id="P5wcsDO]LFoD^l^pN+r{"><field name="NUM">1</field></shadow></value></shadow></value></shadow><block type="mm_println" id="p;~UiogA?2+yE-?f@Pi8"><value name="msg"><shadow type="text" id="I7G`1c?1v}MbxG!_KPY8"><field name="TEXT">___</field></shadow></value><next><block type="mm_return" id="DQj@C,yQ^#e*I_VZN/F!"><value name="ret"><shadow type="math_number" id="P0H8331x+;w|9)E=ZCw]"><field name="NUM">0</field></shadow><block type="mm_var_return" id="Zd3(0k^Z6@Lju[8a=eqE"><field name="NAME">f_z(p)/260</field></block></value></block></next></block></statement><statement name="radius_p"><shadow type="mm_return" id="C9sFT,LX(W3LRo6dO7!l"><value name="ret"><shadow type="math_arithmetic" id="7x[{6rKQ060Gv%a11xK."><field name="OP">MULTIPLY</field><value name="A"><shadow type="mm_var_return" id=":5p-?YbF`bgXFg{PGM!W"><field name="NAME">p</field></shadow></value><value name="B"><shadow type="math_number" id=",pyrGhJH5=M=-jh%I.Q9"><field name="NUM">40</field></shadow></value></shadow></value></shadow></statement></block></value></block></next></block></next></block></next></block></next></block></next></block></next></block></xml>PK
     ��J�:��%  �%     jsmm_scene = null;
mm_scene = new THREE.Scene();
mm_scene.add(new THREE.HemisphereLight('#ffffff', '#666666', 1));
function print_points(polygon){
  for(p of polygon){
      console1.log("("+p.x+","+p.y+"), ");
  }
}
function z_fn(shape, z_n, z_min, z_max){
  var polygon = shape.extractPoints().shape
  //var z_list = []
  var z_values = []

  if(polygon[polygon.length-1].y <= polygon[0].y){
      console.log("ERROR: Polygon sections must have positive slope (1)")
      return function(z){return 0}
  }

  var p_range = polygon[polygon.length-1].y-polygon[0].y
  var z_range = z_max-z_min
  // JCOA: Not sure if the scale is backwards or not needed
  console1.logln("Scale Factor "+scale_factor)
  var y_step = p_range/z_n
  var scale_factor = y_step/z_range

  var f_slope = function(i, pol){
      //console1.logln(pol[i+1].y)
      return (pol[i+1].x-pol[i].x)/(pol[i+1].y-pol[i].y)
  }

  console1.logln(y_step)

  // z_n
  var y = 0
  var p_i = 0
  var slope = f_slope(0, polygon)
  var y_prev = polygon[0].y
  for(let i = 0; i < z_n; i++){
      y = y_step * i
      if(p_i < polygon.length-1){
          if(y >= polygon[p_i+1].y){
              p_i += 1
              y_prev = polygon[p_i].y
              slope = f_slope(p_i, polygon)
              console1.logln("Slope "+slope)
          }
          if(polygon[p_i+1].y <= polygon[p_i].y){
              console.log("ERROR: Polygon sections must have positive slope (2)")
              return function(z){return 0}
          }
          z_values.push([])
          console1.logln("y "+y+" - y_prev "+y_prev)
          let y_dif = y - y_prev
          console1.logln("x "+polygon[p_i].x+" + slope "+slope+" * y_dif "+y_dif)
          let x = polygon[p_i].x + slope * y_dif
          z_values[i].push(x, slope*scale_factor)
      } else {
          z_values.push([])
          z_values[i].push(y, 0)
      }
  }

  // var first = true
  // var prev_y
  // var err = false
  // for(p of polygon){
  //     console1.log("("+p.x+","+p.y+"), ")
  //     z_list.push(p.y)
  //     if(first){
  //         first = false
  //     } else {
  //         if(p.y<= prev_y){
  //             err = true
  //             console1.logln("Error: Only positive slope between points is allowed!")
  //         }
  //     }
  //     prev_y = p.y
  // }

  // if(!err){
  //     if(z<=z_list[0]){
  //         ret_x = polygon[0].x
  //     } else {
  //         if(z>=z_list[z_list.length-1]){
  //             ret_x = polygon[z_list.length-1].x
  //         } else {
  //             let i = 0
  //             while(z > z_list[i]){
  //                 i += 1
  //             }
  //             if(z==z_list[i]){
  //                 ret_x = polygon[i].x
  //             } else {
  //               let p1 = polygon[i-1]
  //               let p2 = polygon[i]
  //               ret_x = p1.x+(z-p1.y)*(p2.x-p1.x)/(p2.y-p1.y)
  //             }
  //         }
  //     }
  // }

  //return ret_x
  // z_min, z_max, z_n, z_range = z_max-z_min
  var z_step = z_range/z_n
  // z_values = [z, slope]
  // We will preload z_n values to this array

  // Convert z_min to polygon y_min and z_max to polygon y_max using scale and offset
  // Loop z_n values from z_min to z_max

  console1.logln(z_values[0])
  console1.logln(z_values[1])
  console1.logln(z_values[2])
  console1.logln(z_values[3])
  console1.logln(z_values[4])
  console1.logln(z_values[5])
  console1.logln(z_values[6])
  console1.logln(z_values[7])
  console1.logln("_____")
  // console1.logln(z_values[8])
  // console1.logln(z_values[9])

  return function(z){
      // Test for z < z_min and z > z_max. Return z_values[0,0] and z_values[z_n-1, 0] respectively
      if(z < z_min){
          return z_values[0][0]
      }
      if(z > z_max){
          return z_values[z_values.length-1][0]
      }

      console1.logln("z "+z)
      var v = (z - z_min)/z_step
      // console1.logln(v)
      var i = Math.floor(v)
      // console1.logln(i)
      var remainder = v - i
      // console1.logln(remainder)
      // console1.logln(z_values[i][0])
      // console1.logln(remainder*z_values[i][1])

      var ret = z_values[i][0] + remainder*z_values[i][1]

      console1.logln("v "+v+" i "+i+" r "+remainder+" ret "+ret)

      //var quotient = Math.floor(y/x);
      //var remainder = y % x;
      return ret
  }












}
var i;


function involuteGear(numTeeth, circularPitch, thickness, hole, twist, pressureAngle, clearance){
  var teeth = mm_set(new THREE.Object3D());
  // Parametric gear derivate from:
  // http://www.thingiverse.com/thing:17584
  // https://github.com/carvadero/OpenJsCsg/blob/gh-pages/src/csg.js

  function add2D(p1, p2){
      return [p1[0]+p2[0], p1[1]+p2[1]];
  }

  function times2D(p, c){
      return [p[0]*c, p[1]*c];
  }

  function normal2D(p){
      return [p[1], -p[0]];
  }

  /*
    For gear terminology see:
      http://www.astronomiainumbria.org/advanced_internet_files/meccanica/easyweb.easynet.co.uk/_chrish/geardata.htm
    Algorithm based on:
      http://www.cartertools.com/involute.html

    circularPitch: The distance between adjacent teeth measured at the pitch circle
  */

  // default values:
  if(arguments.length < 3) thickness = 10;
  if(arguments.length < 4) hole = 0;
  if(arguments.length < 5) twist = 0;
  if(arguments.length < 6) pressureAngle = 20;
  if(arguments.length < 7) clearance = 0;

  var addendum = circularPitch / Math.PI;
  var dedendum = addendum + clearance;

  // radiuses of the 4 circles:
  var pitchRadius = numTeeth * circularPitch / (2 * Math.PI);
  var baseRadius = pitchRadius * Math.cos(Math.PI * pressureAngle / 180);
  var outerRadius = pitchRadius + addendum;
  var rootRadius = pitchRadius - dedendum;

  var maxtanlength = Math.sqrt(outerRadius*outerRadius - baseRadius*baseRadius);
  var maxangle = maxtanlength / baseRadius;

  var tl_at_pitchcircle = Math.sqrt(pitchRadius*pitchRadius - baseRadius*baseRadius);
  var angle_at_pitchcircle = tl_at_pitchcircle / baseRadius;
  var diffangle = angle_at_pitchcircle - Math.atan(angle_at_pitchcircle);
  var angularToothWidthAtBase = Math.PI / numTeeth + 2*diffangle;

  // build a single 2d tooth in the 'points' array:
  var resolution = 5;
  var points = [[0,0]]; // var points = [new CSG.Vector2D(0,0)];
  for(var i = 0; i <= resolution; i++)
  {
      // first side of the tooth:
      var angle = maxangle * i / resolution;
      var tanlength = angle * baseRadius;
      var radvector = [Math.cos(angle), Math.sin(angle)];  // CSG.Vector2D.fromAngle(angle);
      var tanvector = normal2D(radvector); // radvector.normal(); // Rotate 90º Clockwise
      var p = add2D(times2D(radvector, baseRadius), times2D(tanvector, tanlength));  // radvector.times(baseRadius).plus(tanvector.times(tanlength));
      points[i+1] = p;
      console.log(p);

      // opposite side of the tooth:
      var angleOpposite = angularToothWidthAtBase - angle;
      radvector = [Math.cos(angleOpposite), Math.sin(angleOpposite)]; // CSG.Vector2D.fromAngle(angularToothWidthAtBase - angle);
      tanvector = times2D(normal2D(radvector),-1); // radvector.normal().negated();
      p = add2D(times2D(radvector, baseRadius), times2D(tanvector, tanlength));  // radvector.times(baseRadius).plus(tanvector.times(tanlength));
      points[2 * resolution + 2 - i] = p;
      console.log(p);
  }


  var toothShape = mm_polygon(points);


  var tooth = mm_extrude(toothShape, thickness, 4, 0);
  var i_inc = 1;
  if (1 > numTeeth) {
    i_inc = -i_inc;
  }
  for (i = 1;
       i_inc >= 0 ? i <= numTeeth : i >= numTeeth;
       i += i_inc) {
    let angle = i*360/numTeeth;
    teeth.add((tooth.clone())
      .setRotation([0, 0, angle]));
  }
  var rootDiameter = (rootRadius * 2);
  var gear = mm_boolean_op('subtract', mm_boolean_op('union', teeth
    .setTranslation([0, 0, (-thickness / 2)]), mm_cylinder(rootDiameter, rootDiameter, thickness, (numTeeth * 2), 1)), mm_cylinder(hole, hole, (thickness + 2), 32, 1)
    .setTranslation([0, 0, -1]));
  return gear
}
function involuteGearDouble(numTeeth, circularPitch, thickness, hole, twist, pressureAngle, clearance){
  if(arguments.length < 3) thickness = 10;
  if(arguments.length < 4) hole = 0;
  if(arguments.length < 5) twist = 0;
  if(arguments.length < 6) pressureAngle = 20;
  if(arguments.length < 7) clearance = 0;
  var gear = involuteGear(numTeeth, circularPitch, thickness, hole, twist, pressureAngle, clearance);

  mm_twist_fn(gear.geometry,
    function(z){
      let ret;
      if(z<0){
          ret = z*twist/(thickness*360.0);
      } else {
          ret = -z*twist/(thickness*360.0);
      }
      return ret;
    });
  return gear
}
function involuteGearScaleTest(numTeeth, circularPitch, thickness, hole, twist, pressureAngle, clearance){
  if(arguments.length < 3) thickness = 10;
  if(arguments.length < 4) hole = 0;
  if(arguments.length < 5) twist = 0;
  if(arguments.length < 6) pressureAngle = 20;
  if(arguments.length < 7) clearance = 0;
  var gear = involuteGear(numTeeth, circularPitch, thickness, hole, twist, pressureAngle, clearance);

  mm_twist_scale_fn(gear.geometry,
    function(z){
      let ret;
      if(z<0){
          ret = z*twist/(thickness*360.0);
      } else {
          ret = -z*twist/(thickness*360.0);
      }
      return ret;
    },
    function(z){
      return z/thickness+2;
    });
  return gear
}

var f_z = z_fn(mm_polygon([[0,0],[8,8],[1,16],[9,19],[12,24],[7,27],[13,33]]), 200, 0, 1.01);
console1.logln('____');
mm_scene.add(mm_extrude_spiral(80, 400, mm_polygon([[0,0],[10,0],[0,10]]),
function(p){
  console1.logln('___');
  return f_z(p)/260
},
function(p){
  return (p * 40)
}));
PK
     ��Jf=��c%  c%  	   functions{"print_points.js":{"hash":"3d_4af9daab61979b6862bf74fcc2a5630b","name":"print_points.js","data":"for(p of polygon){\n    console1.log(\"(\"+p.x+\",\"+p.y+\"), \");\n}","url":""},"z_fn.js":{"hash":"f06_e972c378d07b35018a5c08c0a982a528","name":"z_fn.js","data":"var polygon = shape.extractPoints().shape\n//var z_list = []\nvar z_values = []\n\nif(polygon[polygon.length-1].y <= polygon[0].y){\n    console.log(\"ERROR: Polygon sections must have positive slope (1)\")\n    return function(z){return 0}\n}\n\nvar p_range = polygon[polygon.length-1].y-polygon[0].y\nvar z_range = z_max-z_min\n// JCOA: Not sure if the scale is backwards or not needed\nconsole1.logln(\"Scale Factor \"+scale_factor)\nvar y_step = p_range/z_n\nvar scale_factor = y_step/z_range\n\nvar f_slope = function(i, pol){\n    //console1.logln(pol[i+1].y)\n    return (pol[i+1].x-pol[i].x)/(pol[i+1].y-pol[i].y)\n}\n\nconsole1.logln(y_step)\n\n// z_n\nvar y = 0\nvar p_i = 0\nvar slope = f_slope(0, polygon)\nvar y_prev = polygon[0].y\nfor(let i = 0; i < z_n; i++){\n    y = y_step * i\n    if(p_i < polygon.length-1){\n        if(y >= polygon[p_i+1].y){\n            p_i += 1\n            y_prev = polygon[p_i].y\n            slope = f_slope(p_i, polygon)\n            console1.logln(\"Slope \"+slope)\n        }\n        if(polygon[p_i+1].y <= polygon[p_i].y){\n            console.log(\"ERROR: Polygon sections must have positive slope (2)\")\n            return function(z){return 0}\n        }\n        z_values.push([])\n        console1.logln(\"y \"+y+\" - y_prev \"+y_prev)\n        let y_dif = y - y_prev\n        console1.logln(\"x \"+polygon[p_i].x+\" + slope \"+slope+\" * y_dif \"+y_dif)\n        let x = polygon[p_i].x + slope * y_dif\n        z_values[i].push(x, slope*scale_factor)\n    } else {\n        z_values.push([])\n        z_values[i].push(y, 0)\n    }\n}\n\n// var first = true\n// var prev_y\n// var err = false\n// for(p of polygon){\n//     console1.log(\"(\"+p.x+\",\"+p.y+\"), \")\n//     z_list.push(p.y)\n//     if(first){\n//         first = false\n//     } else {\n//         if(p.y<= prev_y){\n//             err = true\n//             console1.logln(\"Error: Only positive slope between points is allowed!\")\n//         }\n//     }\n//     prev_y = p.y\n// }\n\n// if(!err){\n//     if(z<=z_list[0]){\n//         ret_x = polygon[0].x\n//     } else {\n//         if(z>=z_list[z_list.length-1]){\n//             ret_x = polygon[z_list.length-1].x\n//         } else {\n//             let i = 0\n//             while(z > z_list[i]){\n//                 i += 1\n//             }\n//             if(z==z_list[i]){\n//                 ret_x = polygon[i].x\n//             } else {\n//               let p1 = polygon[i-1]\n//               let p2 = polygon[i]\n//               ret_x = p1.x+(z-p1.y)*(p2.x-p1.x)/(p2.y-p1.y)\n//             }\n//         }\n//     }\n// }\n\n//return ret_x\n// z_min, z_max, z_n, z_range = z_max-z_min\nvar z_step = z_range/z_n\n// z_values = [z, slope]\n// We will preload z_n values to this array\n\n// Convert z_min to polygon y_min and z_max to polygon y_max using scale and offset\n// Loop z_n values from z_min to z_max\n\nconsole1.logln(z_values[0])\nconsole1.logln(z_values[1])\nconsole1.logln(z_values[2])\nconsole1.logln(z_values[3])\nconsole1.logln(z_values[4])\nconsole1.logln(z_values[5])\nconsole1.logln(z_values[6])\nconsole1.logln(z_values[7])\nconsole1.logln(\"_____\")\n// console1.logln(z_values[8])\n// console1.logln(z_values[9])\n\nreturn function(z){\n    // Test for z < z_min and z > z_max. Return z_values[0,0] and z_values[z_n-1, 0] respectively\n    if(z < z_min){\n        return z_values[0][0]\n    } \n    if(z > z_max){\n        return z_values[z_values.length-1][0]\n    }\n    \n    console1.logln(\"z \"+z)\n    var v = (z - z_min)/z_step\n    // console1.logln(v)\n    var i = Math.floor(v)\n    // console1.logln(i)\n    var remainder = v - i\n    // console1.logln(remainder)\n    // console1.logln(z_values[i][0])\n    // console1.logln(remainder*z_values[i][1])\n    \n    var ret = z_values[i][0] + remainder*z_values[i][1]\n    \n    console1.logln(\"v \"+v+\" i \"+i+\" r \"+remainder+\" ret \"+ret)\n    \n    //var quotient = Math.floor(y/x);\n    //var remainder = y % x;\n    return ret\n}\n\n\n\n\n\n\n\n\n\n\n\n","url":""},"GearLib2.js":{"hash":"13e4_b47b00ebb87d78b9bb61d9ccb7d60e43","name":"GearLib2.js","data":"var i;\n\n\nfunction involuteGear(numTeeth, circularPitch, thickness, hole, twist, pressureAngle, clearance){\n  var teeth = mm_set(new THREE.Object3D());\n  // Parametric gear derivate from:\n  // http://www.thingiverse.com/thing:17584\n  // https://github.com/carvadero/OpenJsCsg/blob/gh-pages/src/csg.js\n\n  function add2D(p1, p2){\n      return [p1[0]+p2[0], p1[1]+p2[1]];\n  }\n\n  function times2D(p, c){\n      return [p[0]*c, p[1]*c];\n  }\n\n  function normal2D(p){\n      return [p[1], -p[0]];\n  }\n\n  /*\n    For gear terminology see:\n      http://www.astronomiainumbria.org/advanced_internet_files/meccanica/easyweb.easynet.co.uk/_chrish/geardata.htm\n    Algorithm based on:\n      http://www.cartertools.com/involute.html\n\n    circularPitch: The distance between adjacent teeth measured at the pitch circle\n  */\n\n  // default values:\n  if(arguments.length < 3) thickness = 10;\n  if(arguments.length < 4) hole = 0;\n  if(arguments.length < 5) twist = 0;\n  if(arguments.length < 6) pressureAngle = 20;\n  if(arguments.length < 7) clearance = 0;\n\n  var addendum = circularPitch / Math.PI;\n  var dedendum = addendum + clearance;\n\n  // radiuses of the 4 circles:\n  var pitchRadius = numTeeth * circularPitch / (2 * Math.PI);\n  var baseRadius = pitchRadius * Math.cos(Math.PI * pressureAngle / 180);\n  var outerRadius = pitchRadius + addendum;\n  var rootRadius = pitchRadius - dedendum;\n\n  var maxtanlength = Math.sqrt(outerRadius*outerRadius - baseRadius*baseRadius);\n  var maxangle = maxtanlength / baseRadius;\n\n  var tl_at_pitchcircle = Math.sqrt(pitchRadius*pitchRadius - baseRadius*baseRadius);\n  var angle_at_pitchcircle = tl_at_pitchcircle / baseRadius;\n  var diffangle = angle_at_pitchcircle - Math.atan(angle_at_pitchcircle);\n  var angularToothWidthAtBase = Math.PI / numTeeth + 2*diffangle;\n\n  // build a single 2d tooth in the 'points' array:\n  var resolution = 5;\n  var points = [[0,0]]; // var points = [new CSG.Vector2D(0,0)];\n  for(var i = 0; i <= resolution; i++)\n  {\n      // first side of the tooth:\n      var angle = maxangle * i / resolution;\n      var tanlength = angle * baseRadius;\n      var radvector = [Math.cos(angle), Math.sin(angle)];  // CSG.Vector2D.fromAngle(angle);\n      var tanvector = normal2D(radvector); // radvector.normal(); // Rotate 90º Clockwise\n      var p = add2D(times2D(radvector, baseRadius), times2D(tanvector, tanlength));  // radvector.times(baseRadius).plus(tanvector.times(tanlength));\n      points[i+1] = p;\n      console.log(p);\n      \n      // opposite side of the tooth:\n      var angleOpposite = angularToothWidthAtBase - angle;\n      radvector = [Math.cos(angleOpposite), Math.sin(angleOpposite)]; // CSG.Vector2D.fromAngle(angularToothWidthAtBase - angle);\n      tanvector = times2D(normal2D(radvector),-1); // radvector.normal().negated();\n      p = add2D(times2D(radvector, baseRadius), times2D(tanvector, tanlength));  // radvector.times(baseRadius).plus(tanvector.times(tanlength));\n      points[2 * resolution + 2 - i] = p;\n      console.log(p);\n  }\n\n\n  var toothShape = mm_polygon(points);\n\n\n  var tooth = mm_extrude(toothShape, thickness, 4, 0);\n  var i_inc = 1;\n  if (1 > numTeeth) {\n    i_inc = -i_inc;\n  }\n  for (i = 1;\n       i_inc >= 0 ? i <= numTeeth : i >= numTeeth;\n       i += i_inc) {\n    let angle = i*360/numTeeth;\n    teeth.add((tooth.clone())\n      .setRotation([0, 0, angle]));\n  }\n  var rootDiameter = (rootRadius * 2);\n  var gear = mm_boolean_op('subtract', mm_boolean_op('union', teeth\n    .setTranslation([0, 0, (-thickness / 2)]), mm_cylinder(rootDiameter, rootDiameter, thickness, (numTeeth * 2), 1)), mm_cylinder(hole, hole, (thickness + 2), 32, 1)\n    .setTranslation([0, 0, -1]));\n  return gear\n}\nfunction involuteGearDouble(numTeeth, circularPitch, thickness, hole, twist, pressureAngle, clearance){\n  if(arguments.length < 3) thickness = 10;\n  if(arguments.length < 4) hole = 0;\n  if(arguments.length < 5) twist = 0;\n  if(arguments.length < 6) pressureAngle = 20;\n  if(arguments.length < 7) clearance = 0;\n  var gear = involuteGear(numTeeth, circularPitch, thickness, hole, twist, pressureAngle, clearance);\n\n  mm_twist_fn(gear.geometry,\n    function(z){\n      let ret;\n      if(z<0){\n          ret = z*twist/(thickness*360.0);\n      } else {\n          ret = -z*twist/(thickness*360.0);\n      }\n      return ret;\n    });\n  return gear\n}\nfunction involuteGearScaleTest(numTeeth, circularPitch, thickness, hole, twist, pressureAngle, clearance){\n  if(arguments.length < 3) thickness = 10;\n  if(arguments.length < 4) hole = 0;\n  if(arguments.length < 5) twist = 0;\n  if(arguments.length < 6) pressureAngle = 20;\n  if(arguments.length < 7) clearance = 0;\n  var gear = involuteGear(numTeeth, circularPitch, thickness, hole, twist, pressureAngle, clearance);\n\n  mm_twist_scale_fn(gear.geometry,\n    function(z){\n      let ret;\n      if(z<0){\n          ret = z*twist/(thickness*360.0);\n      } else {\n          ret = -z*twist/(thickness*360.0);\n      }\n      return ret;\n    },\n    function(z){\n      return z/thickness+2;\n    });\n  return gear\n}\n","url":""}}PK
     ��J            	   textures/PK
     ��JC���         textures/metadata.json{}PK
     ��J               objects/PK
     ��JC���         objects/metadata.json{}PK 
     ��J�dv	   	                    mmpPK 
     ��J��f�r  r               *   xmlPK 
     ��J�:��%  �%               �  jsPK 
     ��Jf=��c%  c%  	             �6  functionsPK 
     ��J            	            \  textures/PK 
     ��JC���                   7\  textures/metadata.jsonPK 
     ��J                        m\  objects/PK 
     ��JC���                   �\  objects/metadata.jsonPK      �  �\    