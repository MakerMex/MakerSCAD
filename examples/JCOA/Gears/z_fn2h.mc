PK
     (��J�dv	   	      mmp{"ui":{}}PK
     (��J,�<�;  ;     xml<xml xmlns="http://www.w3.org/1999/xhtml"><block type="three_init" id="mwYh?BBA3#yQs**}%E*d" x="-233" y="14"><field name="lights">TRUE</field><field name="physijs">FALSE</field><next><block type="bi_named_function" id="P9xfdB4p;g=99K`/!ck]"><field name="function_type">function </field><field name="name">print_points</field><field name="args">polygon</field><statement name="chain"><block type="mm_script_text" id="~agW1EAGzCWZJQ{TaxE["><field name="name">print_points.js</field></block></statement><next><block type="bi_named_function" id="gNo-;At31OE2*#2/E|UV"><field name="function_type">function </field><field name="name">z_fn</field><field name="args">shape, z_n, z_min, z_max</field><statement name="chain"><block type="mm_script_text" id="Wv%iyCA?oWMCF=y8EHAQ"><field name="name">z_fn.js</field></block></statement><next><block type="mm_var" id="rFVYh:X:Y263HP}o/[Y5"><field name="var">f_z</field><value name="val"><block type="bi_call_editable_return" id="+fe,%l6ek,z%lr?]vLLt"><mutation items="5"></mutation><field name="NAME">z_fn</field><value name="items1"><block type="three_polygon" id="!sF8`X,|[1%)V+_ID~9l"><field name="code">[[0,0],[8,8],[4,16]]</field></block></value><value name="items2"><block type="math_number" id=".P?JL!5:2QpYSO51O9%#"><field name="NUM">8</field></block></value><value name="items3"><block type="math_number" id="cqo]*N21+uNCg2Xw?XGu"><field name="NUM">0</field></block></value><value name="items4"><block type="math_number" id="6F@B!]Tu-E#.;SzpUeIJ"><field name="NUM">1.01</field></block></value></block></value><next><block type="mm_println" id="ufulN}M^Z){)+#+HG3Py"><value name="msg"><shadow type="text" id="zuSOgU^mJ[g/HEeiOHxI"><field name="TEXT">____</field></shadow></value><next><block type="three_scene_add" id="AF7qHkQ?~n/GQ?J?P:s-"><value name="mesh"><shadow type="three_box" id="!=_sr@{Q%u2aKdiy[f!:"><value name="size"><shadow type="mm_xyz" id="_Lab|T0XJ?Cm=R-d*yEs"><field name="X">100</field><field name="Y">100</field><field name="Z">100</field></shadow></value><value name="segments"><shadow type="math_number" id="o:TSK;VBm|Yp6co5v*fa"><field name="NUM">1</field></shadow></value></shadow><block type="three_extrude_spiral" id="Q3}{z|^4HZ|J@4q(gOx*"><value name="height"><shadow type="math_number" id="6DH[=oS}Q/Lf@@=)1-0Z"><field name="NUM">80</field></shadow></value><value name="sections"><shadow type="math_number" id="1iz!tZf|x7lG6P[f~?pg"><field name="NUM">400</field></shadow></value><value name="polygon"><shadow type="three_polygon" id="imewXj^q^OeKI^cJHh+h"><field name="code">[[0,0],[10,0],[0,10]]</field></shadow></value><statement name="rotations_p"><shadow type="mm_return" id="#cCmuKMd3RP-r3j0jvhO"><value name="ret"><shadow type="math_arithmetic" id="Zt{#21W,0xP}DdT4*#}}"><field name="OP">MULTIPLY</field><value name="A"><shadow type="mm_var_return" id="tehRCbkmUt7E9!LtAA,q"><field name="NAME">p</field></shadow></value><value name="B"><shadow type="math_number" id="P5wcsDO]LFoD^l^pN+r{"><field name="NUM">1</field></shadow></value></shadow></value></shadow></statement><statement name="radius_p"><shadow type="mm_return" id="C9sFT,LX(W3LRo6dO7!l"><value name="ret"><shadow type="math_arithmetic" id="7x[{6rKQ060Gv%a11xK."><field name="OP">MULTIPLY</field><value name="A"><shadow type="mm_var_return" id=":5p-?YbF`bgXFg{PGM!W"><field name="NAME">p</field></shadow></value><value name="B"><shadow type="math_number" id=",pyrGhJH5=M=-jh%I.Q9"><field name="NUM">40</field></shadow></value></shadow></value></shadow><block type="mm_println" id="p;~UiogA?2+yE-?f@Pi8"><value name="msg"><shadow type="text" id="I7G`1c?1v}MbxG!_KPY8"><field name="TEXT">abc</field></shadow><block type="mm_var_return" id="ix7=+%bh/;N5,/o7S}!~"><field name="NAME">f_z(p)</field></block></value><next><block type="mm_return" id="DQj@C,yQ^#e*I_VZN/F!"><value name="ret"><shadow type="math_number" id="P0H8331x+;w|9)E=ZCw]"><field name="NUM">0</field></shadow><block type="mm_var_return" id="Zd3(0k^Z6@Lju[8a=eqE"><field name="NAME">f_z(p)</field></block></value></block></next></block></statement></block></value></block></next></block></next></block></next></block></next></block></next></block></xml>PK
     (��Jڝ���  �     jsmm_scene = null;
mm_scene = new THREE.Scene();
mm_scene.add(new THREE.HemisphereLight('#ffffff', '#666666', 1));
function print_points(polygon){
  for(p of polygon){
      console1.log("("+p.x+","+p.y+"), ");
  }
}
function z_fn(shape, z_n, z_min, z_max){
  var polygon = shape.extractPoints().shape
  //var z_list = []
  var z_values = []

  if(polygon[polygon.length-1].y <= polygon[0].y){
      console.log("ERROR: Polygon sections must have positive slope (1)")
      return function(z){return 0}
  }

  var p_range = polygon[polygon.length-1].y-polygon[0].y
  var z_range = z_max-z_min
  // JCOA: Not sure if the scale is backwards or not needed
  var scale_factor = p_range/z_range
  var y_step = p_range/z_n

  var f_slope = function(i, pol){
      //console1.logln(pol[i+1].y)
      return (pol[i+1].x-pol[i].x)/(pol[i+1].y-pol[i].y)
  }

  console1.logln(y_step)

  // z_n
  var y = 0
  var p_i = 0
  var slope = f_slope(0, polygon)
  var y_prev = polygon[0].y
  for(let i = 0; i < z_n; i++){
      y = y_step * i
      if(p_i < polygon.length-1){
          if(y >= polygon[p_i+1].y){
              p_i += 1
              y_prev = polygon[p_i+1].y
              slope = f_slope(p_i, polygon)
          }
          if(polygon[p_i+1].y <= polygon[p_i].y){
              console.log("ERROR: Polygon sections must have positive slope (2)")
              return function(z){return 0}
          }
          z_values.push([])
          let y_dif = y - y_prev
          let x = polygon[p_i].x + slope * y_dif
          z_values[i].push(x, slope*scale_factor)
      } else {
          z_values.push([])
          z_values[i].push(y, 0)
      }
  }

  // var first = true
  // var prev_y
  // var err = false
  // for(p of polygon){
  //     console1.log("("+p.x+","+p.y+"), ")
  //     z_list.push(p.y)
  //     if(first){
  //         first = false
  //     } else {
  //         if(p.y<= prev_y){
  //             err = true
  //             console1.logln("Error: Only positive slope between points is allowed!")
  //         }
  //     }
  //     prev_y = p.y
  // }

  // if(!err){
  //     if(z<=z_list[0]){
  //         ret_x = polygon[0].x
  //     } else {
  //         if(z>=z_list[z_list.length-1]){
  //             ret_x = polygon[z_list.length-1].x
  //         } else {
  //             let i = 0
  //             while(z > z_list[i]){
  //                 i += 1
  //             }
  //             if(z==z_list[i]){
  //                 ret_x = polygon[i].x
  //             } else {
  //               let p1 = polygon[i-1]
  //               let p2 = polygon[i]
  //               ret_x = p1.x+(z-p1.y)*(p2.x-p1.x)/(p2.y-p1.y)
  //             }
  //         }
  //     }
  // }

  //return ret_x
  // z_min, z_max, z_n, z_range = z_max-z_min
  var z_step = z_range/z_n
  // z_values = [z, slope]
  // We will preload z_n values to this array

  // Convert z_min to polygon y_min and z_max to polygon y_max using scale and offset
  // Loop z_n values from z_min to z_max

  console1.logln(z_values[0])
  console1.logln(z_values[1])
  console1.logln(z_values[2])
  console1.logln(z_values[3])
  console1.logln(z_values[4])
  console1.logln(z_values[5])
  console1.logln(z_values[6])
  console1.logln(z_values[7])
  console1.logln("_____")
  // console1.logln(z_values[8])
  // console1.logln(z_values[9])

  return function(z){
      // Test for z < z_min and z > z_max. Return z_values[0,0] and z_values[z_n-1, 0] respectively
      if(z < z_min){
          return z_values[0][0]
      }
      if(z > z_max){
          return z_values[z_values.length-1][0]
      }


      var v = (z - z_min)/z_step
      // console1.logln(v)
      var i = Math.floor(v)
      // console1.logln(i)
      var remainder = v - i
      // console1.logln(remainder)
      // console1.logln(z_values[i][0])
      // console1.logln(remainder*z_values[i][1])

      var ret = z_values[i][0] + remainder*z_values[i][1]

      console1.logln("v "+v+" i "+i+" r "+remainder+" ret "+ret)

      //var quotient = Math.floor(y/x);
      //var remainder = y % x;
      return ret
  }












}
var f_z = z_fn(mm_polygon([[0,0],[8,8],[4,16]]), 8, 0, 1.01);
console1.logln('____');
mm_scene.add(mm_extrude_spiral(80, 400, mm_polygon([[0,0],[10,0],[0,10]]),
function(p){
  return (p * 1)
},
function(p){
  console1.logln(f_z(p));
  return f_z(p)
}));
PK
     (��J����  �  	   functions{"print_points.js":{"hash":"3d_4af9daab61979b6862bf74fcc2a5630b","name":"print_points.js","data":"for(p of polygon){\n    console1.log(\"(\"+p.x+\",\"+p.y+\"), \");\n}","url":""},"z_fn.js":{"hash":"d2d_910188e0b57a34767a4771c2b1fce87c","name":"z_fn.js","data":"var polygon = shape.extractPoints().shape\n//var z_list = []\nvar z_values = []\n\nif(polygon[polygon.length-1].y <= polygon[0].y){\n    console.log(\"ERROR: Polygon sections must have positive slope (1)\")\n    return function(z){return 0}\n}\n\nvar p_range = polygon[polygon.length-1].y-polygon[0].y\nvar z_range = z_max-z_min\n// JCOA: Not sure if the scale is backwards or not needed\nvar scale_factor = p_range/z_range\nvar y_step = p_range/z_n\n\nvar f_slope = function(i, pol){\n    //console1.logln(pol[i+1].y)\n    return (pol[i+1].x-pol[i].x)/(pol[i+1].y-pol[i].y)\n}\n\nconsole1.logln(y_step)\n\n// z_n\nvar y = 0\nvar p_i = 0\nvar slope = f_slope(0, polygon)\nvar y_prev = polygon[0].y\nfor(let i = 0; i < z_n; i++){\n    y = y_step * i\n    if(p_i < polygon.length-1){\n        if(y >= polygon[p_i+1].y){\n            p_i += 1\n            y_prev = polygon[p_i+1].y\n            slope = f_slope(p_i, polygon)\n        }\n        if(polygon[p_i+1].y <= polygon[p_i].y){\n            console.log(\"ERROR: Polygon sections must have positive slope (2)\")\n            return function(z){return 0}\n        }\n        z_values.push([])\n        let y_dif = y - y_prev\n        let x = polygon[p_i].x + slope * y_dif\n        z_values[i].push(x, slope*scale_factor)\n    } else {\n        z_values.push([])\n        z_values[i].push(y, 0)\n    }\n}\n\n// var first = true\n// var prev_y\n// var err = false\n// for(p of polygon){\n//     console1.log(\"(\"+p.x+\",\"+p.y+\"), \")\n//     z_list.push(p.y)\n//     if(first){\n//         first = false\n//     } else {\n//         if(p.y<= prev_y){\n//             err = true\n//             console1.logln(\"Error: Only positive slope between points is allowed!\")\n//         }\n//     }\n//     prev_y = p.y\n// }\n\n// if(!err){\n//     if(z<=z_list[0]){\n//         ret_x = polygon[0].x\n//     } else {\n//         if(z>=z_list[z_list.length-1]){\n//             ret_x = polygon[z_list.length-1].x\n//         } else {\n//             let i = 0\n//             while(z > z_list[i]){\n//                 i += 1\n//             }\n//             if(z==z_list[i]){\n//                 ret_x = polygon[i].x\n//             } else {\n//               let p1 = polygon[i-1]\n//               let p2 = polygon[i]\n//               ret_x = p1.x+(z-p1.y)*(p2.x-p1.x)/(p2.y-p1.y)\n//             }\n//         }\n//     }\n// }\n\n//return ret_x\n// z_min, z_max, z_n, z_range = z_max-z_min\nvar z_step = z_range/z_n\n// z_values = [z, slope]\n// We will preload z_n values to this array\n\n// Convert z_min to polygon y_min and z_max to polygon y_max using scale and offset\n// Loop z_n values from z_min to z_max\n\nconsole1.logln(z_values[0])\nconsole1.logln(z_values[1])\nconsole1.logln(z_values[2])\nconsole1.logln(z_values[3])\nconsole1.logln(z_values[4])\nconsole1.logln(z_values[5])\nconsole1.logln(z_values[6])\nconsole1.logln(z_values[7])\nconsole1.logln(\"_____\")\n// console1.logln(z_values[8])\n// console1.logln(z_values[9])\n\nreturn function(z){\n    // Test for z < z_min and z > z_max. Return z_values[0,0] and z_values[z_n-1, 0] respectively\n    if(z < z_min){\n        return z_values[0][0]\n    } \n    if(z > z_max){\n        return z_values[z_values.length-1][0]\n    }\n    \n\n    var v = (z - z_min)/z_step\n    // console1.logln(v)\n    var i = Math.floor(v)\n    // console1.logln(i)\n    var remainder = v - i\n    // console1.logln(remainder)\n    // console1.logln(z_values[i][0])\n    // console1.logln(remainder*z_values[i][1])\n    \n    var ret = z_values[i][0] + remainder*z_values[i][1]\n    \n    console1.logln(\"v \"+v+\" i \"+i+\" r \"+remainder+\" ret \"+ret)\n    \n    //var quotient = Math.floor(y/x);\n    //var remainder = y % x;\n    return ret\n}\n\n\n\n\n\n\n\n\n\n\n\n","url":""}}PK
     (��J            	   textures/PK
     (��JC���         textures/metadata.json{}PK
     (��J               objects/PK
     (��JC���         objects/metadata.json{}PK 
     (��J�dv	   	                    mmpPK 
     (��J,�<�;  ;               *   xmlPK 
     (��Jڝ���  �               �  jsPK 
     (��J����  �  	             �!  functionsPK 
     (��J            	            �1  textures/PK 
     (��JC���                   �1  textures/metadata.jsonPK 
     (��J                        �1  objects/PK 
     (��JC���                   2  objects/metadata.jsonPK      �  G2    