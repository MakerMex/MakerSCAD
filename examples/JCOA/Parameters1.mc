PK
     ��zL���         mmp{"ui":{"val1":70}}PK
     ��zL���P\  \     xml<xml xmlns="http://www.w3.org/1999/xhtml"><block type="mm_var" id="|:NOT9.gbyf(XYWGNBwu" x="-183" y="-154"><field name="var">z</field><value name="val"><block type="math_number" id="v.=_mUDkhLf-@7[]qigC"><field name="NUM">0</field></block></value><next><block type="mm_gui_init" id="80fFJaf!9AfMLXy.oN`;"><next><block type="mm_gui_value" id="qovsm/0!Y*MJ[Q{M7nDg"><field name="name">val1</field><field name="min">0</field><field name="max">100</field><field name="step">10</field><statement name="chain"><block type="mm_set_to" id="+.6;HI/`E1`J+J^SGSE~"><field name="code">z</field><value name="input"><block type="mm_var_return" id="](3Z_t|YXgw@qeB!BW~j"><field name="NAME">val</field></block></value></block></statement><next><block type="three_init" id="4gA1:|-q7v4L0Lc-D:9*"><field name="lights">TRUE</field><field name="physijs">FALSE</field><next><block type="three_scene_add" id="jCE|rDk)-C,|/g(fuFLl"><value name="mesh"><shadow type="three_box" id=":_00bSaTT91W2hU`RTi0"><value name="size"><shadow type="mm_xyz" id="f[,Jd7D3RRd?6d0CGa6Y"><field name="X">200</field><field name="Y">200</field><field name="Z">10</field></shadow><block type="mm_list_xyz" id="24u4LL`7f(fK`Ii3SJO%"><value name="X"><shadow type="math_number" id="{x~pBjYG(j:]0q3,^es1"><field name="NUM">200</field></shadow></value><value name="Y"><shadow type="math_number" id="c1`l2wb^8O8YaOm*{DFE"><field name="NUM">200</field></shadow></value><value name="Z"><shadow type="math_number" id="j?v(eGZprz6}`aZb+)s`"><field name="NUM">0</field></shadow><block type="mm_var_return" id="iM4D=ZAfxDy6,R(bWbj="><field name="NAME">mmp.ui.val1</field></block></value></block></value><value name="segments"><shadow type="math_number" id="[vp8?N!%!b7T01;~)@*]"><field name="NUM">1</field></shadow></value></shadow></value><next><block type="three_scene_add" id="ORKEj(^bJp+F`dgD+J-;"><value name="mesh"><shadow type="three_box" id="/Y*ZR:f:Bz?@`WE(^8Or"><value name="size"><shadow type="mm_xyz" id="XG),@U0jWT;%eXg^dr-v"><field name="X">100</field><field name="Y">100</field><field name="Z">100</field></shadow></value><value name="segments"><shadow type="math_number" id="SmnGz?(cA!^4:UU:ctd%"><field name="NUM">1</field></shadow></value></shadow><block type="three_translate" id="E]vm#ZEi:`sZ(W=lw+8F"><value name="coordinates"><shadow type="mm_xyz" id="9PX1=eyXw5QvJR0(6)fv"><field name="X">0</field><field name="Y">0</field><field name="Z">20</field></shadow></value><value name="object"><shadow type="three_box" id="V_!LJ+NXX!r,6j%a_xX*"><value name="size"><shadow type="mm_xyz" id="UYUeD{%3pykM]Ok!?i6u"><field name="X">20</field><field name="Y">20</field><field name="Z">40</field></shadow></value><value name="segments"><shadow type="math_number" id="Vq(S091rS9ErDsQ3RFi,"><field name="NUM">1</field></shadow></value></shadow></value></block></value></block></next></block></next></block></next></block></next></block></next></block></xml>PK
     ��zL�t�P         jsvar z = 0;
mm.gui_params = {};
mm.gui = new dat.GUI();
mmp.ui.val1 = mmp.ui.val1 || 0.0;
mm.gui_params.val1 = mmp.ui.val1;
mm.gui.add(mm.gui_params, 'val1', 0, 100).step(10)
  .onChange(function (val){
    mmp.ui.val1 = val;
    z = val;
  });
mm_scene = null;
mm_scene = new THREE.Scene();
mm_scene.add(new THREE.HemisphereLight('#ffffff', '#666666', 1));
mm_scene.add(mm_new_mesh(new THREE.BoxGeometry( 200, 200, mmp.ui.val1, 1, 1, 1 )));
mm_scene.add(mm_new_mesh(new THREE.BoxGeometry( 20, 20, 40, 1, 1, 1 ))
  .setTranslation([0, 0, 20]));
PK
     ��zL�z.c   c   	   functions{"script1.js":{"hash":"0_d41d8cd98f00b204e9800998ecf8427e","name":"script1.js","data":"","url":""}}PK
     ��zL            	   textures/PK
     ��zLC���         textures/metadata.json{}PK
     ��zL               objects/PK
     ��zLC���         objects/metadata.json{}PK 
     ��zL���                       mmpPK 
     ��zL���P\  \               3   xmlPK 
     ��zL�t�P                   �  jsPK 
     ��zL�z.c   c   	             �  functionsPK 
     ��zL            	            z  textures/PK 
     ��zLC���                   �  textures/metadata.jsonPK 
     ��zL                        �  objects/PK 
     ��zLC���                   �  objects/metadata.jsonPK      �  2    