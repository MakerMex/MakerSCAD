var i;
var j;
var k;

function math_random_int(a, b) {
  if (a > b) {
    // Swap a and b to ensure a is smaller.
    var c = a;
    a = b;
    b = c;
  }
  return Math.floor(Math.random() * (b - a + 1) + a);
}

function colour_random() {
  var num = Math.floor(Math.random() * Math.pow(2, 24));
  return '#' + ('00000' + num.toString(16)).substr(-6);
}


var max = 10;
mm_scene = null;
mm_scene = new THREE.Scene();
mm_scene.add(new THREE.HemisphereLight('#ffffff', '#666666', 1));
var i_inc = 1;
if (1 > max) {
  i_inc = -i_inc;
}
for (i = 1;
     i_inc >= 0 ? i <= max : i >= max;
     i += i_inc) {
  var j_inc = 1;
  if (1 > max) {
    j_inc = -j_inc;
  }
  for (j = 1;
       j_inc >= 0 ? j <= max : j >= max;
       j += j_inc) {
    var k_inc = 1;
    if (1 > max) {
      k_inc = -k_inc;
    }
    for (k = 1;
         k_inc >= 0 ? k <= max : k >= max;
         k += k_inc) {
      if ((i == 1 || i == max) || ((j == 1 || j == max) || (k == 1 || k == max))) {
        var s1 = (20 + math_random_int(1, 20));
        mm_scene.add(mm_new_mesh(new THREE.BoxGeometry( s1, s1, s1, 1, 1, 1 ))
          .setColor((colour_random()))
          .setTranslation([30*i, 30*j, 30*k]));
      }
    }
  }
}
