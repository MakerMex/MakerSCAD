PK
     3j�L�dv	   	      mmp{"ui":{}}PK
     3j�L�m��  �     xml<xml xmlns="http://www.w3.org/1999/xhtml"><block type="three_init" id="5|:.[o@|u].:R1jYhR4m" x="9" y="3"><field name="lights">TRUE</field><field name="physijs">FALSE</field><next><block type="three_scene_add" id="KbW](Z/jmot|HAsmhNPi"><value name="mesh"><block type="three_rotate" id="G|MF#ueC`sp.c[b4P8?t"><value name="coordinates"><shadow type="mm_xyz" id="~9u1?SkM[[.LXK{#s?st"><field name="X">270</field><field name="Y">0</field><field name="Z">0</field></shadow></value><value name="object"><shadow type="three_box" id="!62J7Y7-WiV5dY9F@sc@"><value name="size"><shadow type="mm_xyz" id="8)?SUPZmY_V5ylYGvQtP"><field name="X">100</field><field name="Y">100</field><field name="Z">100</field></shadow></value><value name="segments"><shadow type="math_number" id="em~q:wi+SxVwykKgzbeE"><field name="NUM">1</field></shadow></value></shadow><block type="three_group" id="gdy4iJir8~zm:7Zo)!n."><mutation items="3"></mutation><value name="items1"><block type="three_setcolor" id="/f]+CGfi_Qny1X{Idq1G"><value name="color"><block type="colour_picker" id="FFW:Gp]!B0rz1JY%JzWa"><field name="COLOUR">#ff0000</field></block></value><value name="object"><block type="three_part" id="C=.Kn3LZ7e?kVI(:7t=*"><field name="boolean_op">subtract</field><value name="block1"><shadow type="three_box" id="CD5p4QnE-HEv1V=L#p^i"><value name="size"><shadow type="mm_xyz" id="b.)]^C04r5~Jq@M9C4sh"><field name="X">100</field><field name="Y">100</field><field name="Z">100</field></shadow></value><value name="segments"><shadow type="math_number" id="mkk_;PwmRwh/I]fM7[^+"><field name="NUM">1</field></shadow></value></shadow></value><value name="block2"><shadow type="three_sphere" id="pXgTfVv!.{QVA.rSRVDd"><value name="diameter"><shadow type="math_number" id="+E0etD8tx!d0U)-aSse-"><field name="NUM">120</field></shadow></value><value name="resolution"><shadow type="math_number" id="W^_N@(tt`cM6kiUr^tJK"><field name="NUM">32</field></shadow></value></shadow></value></block></value></block></value><value name="items2"><block type="three_translate" id="ju`M0E=!F_JhAK/*V#Sn"><value name="coordinates"><block type="mm_xyz" id="?f!l-R+@tuYXe,uMt3FO"><field name="X">100</field><field name="Y">0</field><field name="Z">0</field></block></value><value name="object"><block type="three_cylinder" id="cCS2=1P:8d`]3ZI]eTZ)"><value name="diameter1"><shadow type="math_number" id="q~](#(~:pUa/5Qm}*.G."><field name="NUM">0</field></shadow></value><value name="diameter2"><shadow type="math_number" id=":Rs%=%C@`zwy!h4rGO2x"><field name="NUM">50</field></shadow></value><value name="height"><shadow type="math_number" id="RDR9urc6himD*D~ojWFO"><field name="NUM">100</field></shadow></value><value name="sides"><shadow type="math_number" id="klH@cK!Sem:jc7sd#j+P"><field name="NUM">32</field></shadow></value></block></value></block></value></block></value></block></value><next><block type="mm_code_line" id=")W`,]Q-#TDtyb4p`r~(i"><field name="code"></field></block></next></block></next></block></xml>PK
     3j�L���  �     jsmm_scene = null;
mm_scene = new THREE.Scene();
mm_scene.add(new THREE.HemisphereLight('#ffffff', '#666666', 1));
mm_scene.add(mm_set(new THREE.Object3D().addMeshList([mm_boolean_op('subtract', mm_new_mesh(new THREE.BoxGeometry( 100, 100, 100, 1, 1, 1 )), mm_new_mesh(new THREE.SphereGeometry( 120/2, 32, 32 )))
  .setColor('#ff0000'), mm_cylinder(0, 50, 100, 32, 1)
  .setTranslation([100, 0, 0])]))
  .setRotation([270, 0, 0]));
PK
     3j�L�z.c   c   	   functions{"script1.js":{"hash":"0_d41d8cd98f00b204e9800998ecf8427e","name":"script1.js","data":"","url":""}}PK
     3j�L            	   textures/PK
     3j�LC���         textures/metadata.json{}PK
     3j�L               objects/PK
     3j�LC���         objects/metadata.json{}PK 
     3j�L�dv	   	                    mmpPK 
     3j�L�m��  �               *   xmlPK 
     3j�L���  �               �  jsPK 
     3j�L�z.c   c   	             �  functionsPK 
     3j�L            	            C  textures/PK 
     3j�LC���                   j  textures/metadata.jsonPK 
     3j�L                        �  objects/PK 
     3j�LC���                   �  objects/metadata.jsonPK      �  �    