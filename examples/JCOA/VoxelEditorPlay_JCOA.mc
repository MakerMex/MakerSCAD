PK
     �t�L�Q��S   S      mmp{"data":[[62.5,-62.5,12.5],[-87.5,-12.5,12.5],[-37.5,62.5,12.5],[112.5,62.5,12.5]]}PK
     �t�L����m  m     xml<xml xmlns="http://www.w3.org/1999/xhtml"><block type="three_play_init" id="wS:DCC`6l7S|rknAr;NW" x="51" y="149"><statement name="st"><block type="three_init" id="S{.1dw/lCRIe]5;AeblO"><field name="lights">TRUE</field><field name="physijs">FALSE</field><next><block type="mm_script_text" id="*:!n^ro(1G3_a2OL5AKZ"><field name="name">voxelpainter.js</field></block></next></block></statement></block><block type="three_play_close" id=",U;H:o4kT97pbBZv.J:C" x="49" y="313"><statement name="st"><block type="mm_script_text" id="nC0g:k6VBF(eG)Q{ooad"><field name="name">play_close.js</field></block></statement></block></xml>PK
     �t�L��E)�"  �"     jsmm_play_init = function(){
  mm_scene = null;
  mm_scene = new THREE.Scene();
  mm_scene.add(new THREE.HemisphereLight('#ffffff', '#666666', 1));
  var container;
  var camera, scene, renderer;
  var plane, cube;
  var mouse, raycaster, isShiftDown = false;

  var rollOverMesh, rollOverMaterial;
  var cubeGeo, cubeMaterial;

  var objects = [];

  if(mmp.data){
      //console.log("DEBUG: mmp['data'], true");
  } else {
      //console.log("DEBUG: mmp['data'], false");
      mmp.data = []; // objects;
  }

  //if(mmt.objects){
  //} else {
  mmt.objects = objects;
  //}

  mm.init = function() {

  //    container = document.createElement( 'div' );
  //    document.body.appendChild( container );
  //
  //    var info = document.createElement( 'div' );
  //    info.style.position = 'absolute';
  //    info.style.top = '10px';
  //    info.style.width = '100%';
  //    info.style.textAlign = 'center';
  //    info.innerHTML = '<a href="http://threejs.org" target="_blank">three.js</a> - voxel painter - webgl<br><strong>click</strong>: add voxel, <strong>shift + click</strong>: remove voxel';
  //    container.appendChild( info );
    
      mmt.scale = 0.5;
    
      var three_container = document.getElementById("three-container");
    
      mm_camera = new THREE.PerspectiveCamera( 45, three_container.clientWidth / three_container.clientHeight, 1, 10000 );
      mm_camera.position.set( 500*mmt.scale, 800*mmt.scale, 1300*mmt.scale );
      mm_camera.up = new THREE.Vector3( 0, 0, 1 );
      mm_camera.lookAt( new THREE.Vector3() );

      mm_scene = new THREE.Scene();

      // roll-over helpers

      rollOverGeo = new THREE.BoxGeometry( 50*mmt.scale, 50*mmt.scale, 50*mmt.scale );
      rollOverMaterial = new THREE.MeshBasicMaterial( { color: 0xff0000, opacity: 0.5, transparent: true } );
      rollOverMesh = new THREE.Mesh( rollOverGeo, rollOverMaterial );
      mm_scene.add( rollOverMesh );

      // cubes

      cubeGeo = new THREE.BoxGeometry( 50*mmt.scale, 50*mmt.scale, 50*mmt.scale );
      cubeMaterial = new THREE.MeshLambertMaterial( { color: 0xfeb74c}); //, map: new THREE.TextureLoader().load( "textures/square-outline-textured.png" ) } );

      // grid

      var size = 500*mmt.scale, step = 50*mmt.scale;

      var geometry = new THREE.Geometry();

      for ( var i = - size; i <= size; i += step ) {

          geometry.vertices.push( new THREE.Vector3( - size, i, 0 ) );
          geometry.vertices.push( new THREE.Vector3(   size, i, 0 ) );

          geometry.vertices.push( new THREE.Vector3( i, - size, 0 ) );
          geometry.vertices.push( new THREE.Vector3( i, size, 0 ) );

      }

      var material = new THREE.LineBasicMaterial( { color: 0xffffff, opacity: 0.2, transparent: true } );

      var line = new THREE.LineSegments( geometry, material );
      mm_scene.add( line );

      //

      raycaster = new THREE.Raycaster();
      mouse = new THREE.Vector2();

      geometry = new THREE.PlaneBufferGeometry( 1000, 1000 );
      //geometry.rotateX( - Math.PI / 2 );

      plane = new THREE.Mesh( geometry, new THREE.MeshBasicMaterial( { visible: false } ) );

      mm_scene.add( plane );
      objects.push( plane );

      // Lights

      var ambientLight = new THREE.AmbientLight( 0x606060 );
      mm_scene.add( ambientLight );

      var directionalLight = new THREE.DirectionalLight( 0xffffff );
      directionalLight.position.set( 1, 0.75, 0.5 ).normalize();
      mm_scene.add( directionalLight );

      mmt.length = objects.length;
      if(mmp.data.length > 0){
          for(var j in mmp.data){
              //console.log(objects[j]);
              var voxel = new THREE.Mesh( cubeGeo, cubeMaterial );
              voxel.position.fromArray(mmp.data[j]); //(new THREE.Vector3(mmp.data[j]));
              objects.push(voxel);
              mm_scene.add(voxel);
          }
      }

  //    renderer = new THREE.WebGLRenderer( { antialias: true } );
  //    renderer.setClearColor( 0xf0f0f0 );
  //    renderer.setPixelRatio( window.devicePixelRatio );
  //    renderer.setSize( window.innerWidth, window.innerHeight );
  //    container.appendChild( renderer.domElement );

      document.addEventListener( 'mousemove', mm.onDocumentMouseMove, false );
      document.addEventListener( 'mousedown', mm.onDocumentMouseDown, false );
      document.addEventListener( 'keydown', mm.onDocumentKeyDown, false );
      document.addEventListener( 'keyup', mm.onDocumentKeyUp, false );

      mmt.voxelRemoveEvents = function(){
          document.removeEventListener( 'mousemove', mm.onDocumentMouseMove, false );
          document.removeEventListener( 'mousedown', mm.onDocumentMouseDown, false );
          document.removeEventListener( 'keydown', mm.onDocumentKeyDown, false );
          document.removeEventListener( 'keyup', mm.onDocumentKeyUp, false );
      };

      //

      //window.addEventListener( 'resize', onWindowResize, false );

  };

  //function onWindowResize() {
  //
  //    mm_camera.aspect = window.innerWidth / window.innerHeight;
  //    mm_camera.updateProjectionMatrix();
  //
  //    renderer.setSize( window.innerWidth, window.innerHeight );
  //
  //}

  mm.onDocumentMouseMove = function( event ) {

      event.preventDefault();

      mouse.set( ( event.clientX / window.innerWidth ) * 2 - 1, - ( event.clientY / window.innerHeight ) * 2 + 1 );

      raycaster.setFromCamera( mouse, mm_camera );

      var intersects = raycaster.intersectObjects( objects );

      if ( intersects.length > 0 ) {

          var intersect = intersects[ 0 ];

          rollOverMesh.position.copy( intersect.point ).add( intersect.face.normal );
          rollOverMesh.position.divideScalar( 50*mmt.scale ).floor().multiplyScalar( 50*mmt.scale ).addScalar( 25*mmt.scale );

      }

      //render();

  };

  mm.onDocumentMouseDown = function( event ) {

      event.preventDefault();

      mouse.set( ( event.clientX / window.innerWidth ) * 2 - 1, - ( event.clientY / window.innerHeight ) * 2 + 1 );

      raycaster.setFromCamera( mouse, mm_camera );

      var intersects = raycaster.intersectObjects( objects );

      if ( intersects.length > 0 ) {

          var intersect = intersects[ 0 ];

          // delete cube

          if ( isShiftDown ) {

              if ( intersect.object != plane ) {

                  mm_scene.remove( intersect.object );

                  objects.splice( objects.indexOf( intersect.object ), 1 );

              }

          // create cube

          } else {

              var voxel = new THREE.Mesh( cubeGeo, cubeMaterial );
              voxel.position.copy( intersect.point ).add( intersect.face.normal );
              voxel.position.divideScalar( 50*mmt.scale ).floor().multiplyScalar( 50*mmt.scale ).addScalar( 25*mmt.scale );
              
              console.log(voxel.position.toArray());

              mm_scene.add( voxel );
              //mmp.data.push( voxel.position.toArray() );
              objects.push( voxel );

          }

          //render();

      }

  };

  mm.onDocumentKeyDown = function( event ) {

      switch( event.keyCode ) {

          case 16: isShiftDown = true; break;

      }

  };

  mm.onDocumentKeyUp = function( event ) {

      switch ( event.keyCode ) {

          case 16: isShiftDown = false; break;

      }

  };

  //function render() {
  //
  //    renderer.render( mm_scene, mm_camera );
  //
  //}

  mm.init();
  //render();

}
mm_play_close = function(){
  mmt.voxelRemoveEvents();

  console.log(mmt.objects.length);

  mmp.data = [];
  if(mmt.objects){
      for(var i = mmt.length; i<mmt.objects.length; i++){
          mmp.data.push(mmt.objects[i].position.toArray());
      }
  }

  var three_container = document.getElementById("three-container");

  mm_camera = new THREE.OrthographicCamera( three_container.clientWidth / - 2, three_container.clientWidth / 2, three_container.clientHeight / 2, three_container.clientHeight / - 2, 1, 1000 );
  //}
  //t/mm_camera = new THREE.PerspectiveCamera( 75, box2.clientWidth / (box2.clientHeight), 1, 1000 );
  //mm_camera = new THREE.OrthographicCamera( three_container.clientWidth / - 2, three_container.clientWidth / 2, three_container.clientHeight / 2, three_container.clientHeight / - 2, 1, 1000 );
  //mm_camera.position.z = 500;
  mm_camera.position.y = 200;
  mm_camera.position.x = 200;
  mm_camera.position.z = 100;
  mm_camera.up = new THREE.Vector3( 0, 0, 1 );
  mm_camera.lookAt(new THREE.Vector3( 0, 0, 0 ));

  mm_clock = new THREE.Clock();
  mm_controls = new THREE.OrbitControls( mm_camera, mm_renderer.domElement );

}PK
     �t�Lc���E#  E#  	   functions{"voxelpainter.js":{"hash":"1aee_3fb96cdfd3cc23d455b936390ef88188","name":"voxelpainter.js","data":"var container;\r\nvar camera, scene, renderer;\r\nvar plane, cube;\r\nvar mouse, raycaster, isShiftDown = false;\r\n\r\nvar rollOverMesh, rollOverMaterial;\r\nvar cubeGeo, cubeMaterial;\r\n\r\nvar objects = [];\r\n\r\nif(mmp.data){\r\n    //console.log(\"DEBUG: mmp['data'], true\");\r\n} else {\r\n    //console.log(\"DEBUG: mmp['data'], false\");\r\n    mmp.data = []; // objects;\r\n}\r\n\r\n//if(mmt.objects){\r\n//} else {\r\nmmt.objects = objects;\r\n//}\r\n\r\nmm.init = function() {\r\n\r\n//    container = document.createElement( 'div' );\r\n//    document.body.appendChild( container );\r\n//\r\n//    var info = document.createElement( 'div' );\r\n//    info.style.position = 'absolute';\r\n//    info.style.top = '10px';\r\n//    info.style.width = '100%';\r\n//    info.style.textAlign = 'center';\r\n//    info.innerHTML = '<a href=\"http://threejs.org\" target=\"_blank\">three.js</a> - voxel painter - webgl<br><strong>click</strong>: add voxel, <strong>shift + click</strong>: remove voxel';\r\n//    container.appendChild( info );\r\n  \r\n    mmt.scale = 0.5;\r\n  \r\n    var three_container = document.getElementById(\"three-container\");\r\n  \r\n    mm_camera = new THREE.PerspectiveCamera( 45, three_container.clientWidth / three_container.clientHeight, 1, 10000 );\r\n    mm_camera.position.set( 500*mmt.scale, 800*mmt.scale, 1300*mmt.scale );\r\n    mm_camera.up = new THREE.Vector3( 0, 0, 1 );\r\n    mm_camera.lookAt( new THREE.Vector3() );\r\n\r\n    mm_scene = new THREE.Scene();\r\n\r\n    // roll-over helpers\r\n\r\n    rollOverGeo = new THREE.BoxGeometry( 50*mmt.scale, 50*mmt.scale, 50*mmt.scale );\r\n    rollOverMaterial = new THREE.MeshBasicMaterial( { color: 0xff0000, opacity: 0.5, transparent: true } );\r\n    rollOverMesh = new THREE.Mesh( rollOverGeo, rollOverMaterial );\r\n    mm_scene.add( rollOverMesh );\r\n\r\n    // cubes\r\n\r\n    cubeGeo = new THREE.BoxGeometry( 50*mmt.scale, 50*mmt.scale, 50*mmt.scale );\r\n    cubeMaterial = new THREE.MeshLambertMaterial( { color: 0xfeb74c}); //, map: new THREE.TextureLoader().load( \"textures/square-outline-textured.png\" ) } );\r\n\r\n    // grid\r\n\r\n    var size = 500*mmt.scale, step = 50*mmt.scale;\r\n\r\n    var geometry = new THREE.Geometry();\r\n\r\n    for ( var i = - size; i <= size; i += step ) {\r\n\r\n        geometry.vertices.push( new THREE.Vector3( - size, i, 0 ) );\r\n        geometry.vertices.push( new THREE.Vector3(   size, i, 0 ) );\r\n\r\n        geometry.vertices.push( new THREE.Vector3( i, - size, 0 ) );\r\n        geometry.vertices.push( new THREE.Vector3( i, size, 0 ) );\r\n\r\n    }\r\n\r\n    var material = new THREE.LineBasicMaterial( { color: 0xffffff, opacity: 0.2, transparent: true } );\r\n\r\n    var line = new THREE.LineSegments( geometry, material );\r\n    mm_scene.add( line );\r\n\r\n    //\r\n\r\n    raycaster = new THREE.Raycaster();\r\n    mouse = new THREE.Vector2();\r\n\r\n    geometry = new THREE.PlaneBufferGeometry( 1000, 1000 );\r\n    //geometry.rotateX( - Math.PI / 2 );\r\n\r\n    plane = new THREE.Mesh( geometry, new THREE.MeshBasicMaterial( { visible: false } ) );\r\n\r\n    mm_scene.add( plane );\r\n    objects.push( plane );\r\n\r\n    // Lights\r\n\r\n    var ambientLight = new THREE.AmbientLight( 0x606060 );\r\n    mm_scene.add( ambientLight );\r\n\r\n    var directionalLight = new THREE.DirectionalLight( 0xffffff );\r\n    directionalLight.position.set( 1, 0.75, 0.5 ).normalize();\r\n    mm_scene.add( directionalLight );\r\n\r\n    mmt.length = objects.length;\r\n    if(mmp.data.length > 0){\r\n        for(var j in mmp.data){\r\n            //console.log(objects[j]);\r\n            var voxel = new THREE.Mesh( cubeGeo, cubeMaterial );\r\n            voxel.position.fromArray(mmp.data[j]); //(new THREE.Vector3(mmp.data[j]));\r\n            objects.push(voxel);\r\n            mm_scene.add(voxel);\r\n        }\r\n    }\r\n\r\n//    renderer = new THREE.WebGLRenderer( { antialias: true } );\r\n//    renderer.setClearColor( 0xf0f0f0 );\r\n//    renderer.setPixelRatio( window.devicePixelRatio );\r\n//    renderer.setSize( window.innerWidth, window.innerHeight );\r\n//    container.appendChild( renderer.domElement );\r\n\r\n    document.addEventListener( 'mousemove', mm.onDocumentMouseMove, false );\r\n    document.addEventListener( 'mousedown', mm.onDocumentMouseDown, false );\r\n    document.addEventListener( 'keydown', mm.onDocumentKeyDown, false );\r\n    document.addEventListener( 'keyup', mm.onDocumentKeyUp, false );\r\n\r\n    mmt.voxelRemoveEvents = function(){\r\n        document.removeEventListener( 'mousemove', mm.onDocumentMouseMove, false );\r\n        document.removeEventListener( 'mousedown', mm.onDocumentMouseDown, false );\r\n        document.removeEventListener( 'keydown', mm.onDocumentKeyDown, false );\r\n        document.removeEventListener( 'keyup', mm.onDocumentKeyUp, false );\r\n    };\r\n\r\n    //\r\n\r\n    //window.addEventListener( 'resize', onWindowResize, false );\r\n\r\n};\r\n\r\n//function onWindowResize() {\r\n//\r\n//    mm_camera.aspect = window.innerWidth / window.innerHeight;\r\n//    mm_camera.updateProjectionMatrix();\r\n//\r\n//    renderer.setSize( window.innerWidth, window.innerHeight );\r\n//\r\n//}\r\n\r\nmm.onDocumentMouseMove = function( event ) {\r\n\r\n    event.preventDefault();\r\n\r\n    mouse.set( ( event.clientX / window.innerWidth ) * 2 - 1, - ( event.clientY / window.innerHeight ) * 2 + 1 );\r\n\r\n    raycaster.setFromCamera( mouse, mm_camera );\r\n\r\n    var intersects = raycaster.intersectObjects( objects );\r\n\r\n    if ( intersects.length > 0 ) {\r\n\r\n        var intersect = intersects[ 0 ];\r\n\r\n        rollOverMesh.position.copy( intersect.point ).add( intersect.face.normal );\r\n        rollOverMesh.position.divideScalar( 50*mmt.scale ).floor().multiplyScalar( 50*mmt.scale ).addScalar( 25*mmt.scale );\r\n\r\n    }\r\n\r\n    //render();\r\n\r\n};\r\n\r\nmm.onDocumentMouseDown = function( event ) {\r\n\r\n    event.preventDefault();\r\n\r\n    mouse.set( ( event.clientX / window.innerWidth ) * 2 - 1, - ( event.clientY / window.innerHeight ) * 2 + 1 );\r\n\r\n    raycaster.setFromCamera( mouse, mm_camera );\r\n\r\n    var intersects = raycaster.intersectObjects( objects );\r\n\r\n    if ( intersects.length > 0 ) {\r\n\r\n        var intersect = intersects[ 0 ];\r\n\r\n        // delete cube\r\n\r\n        if ( isShiftDown ) {\r\n\r\n            if ( intersect.object != plane ) {\r\n\r\n                mm_scene.remove( intersect.object );\r\n\r\n                objects.splice( objects.indexOf( intersect.object ), 1 );\r\n\r\n            }\r\n\r\n        // create cube\r\n\r\n        } else {\r\n\r\n            var voxel = new THREE.Mesh( cubeGeo, cubeMaterial );\r\n            voxel.position.copy( intersect.point ).add( intersect.face.normal );\r\n            voxel.position.divideScalar( 50*mmt.scale ).floor().multiplyScalar( 50*mmt.scale ).addScalar( 25*mmt.scale );\r\n            \r\n            console.log(voxel.position.toArray());\r\n\r\n            mm_scene.add( voxel );\r\n            //mmp.data.push( voxel.position.toArray() );\r\n            objects.push( voxel );\r\n\r\n        }\r\n\r\n        //render();\r\n\r\n    }\r\n\r\n};\r\n\r\nmm.onDocumentKeyDown = function( event ) {\r\n\r\n    switch( event.keyCode ) {\r\n\r\n        case 16: isShiftDown = true; break;\r\n\r\n    }\r\n\r\n};\r\n\r\nmm.onDocumentKeyUp = function( event ) {\r\n\r\n    switch ( event.keyCode ) {\r\n\r\n        case 16: isShiftDown = false; break;\r\n\r\n    }\r\n\r\n};\r\n\r\n//function render() {\r\n//\r\n//    renderer.render( mm_scene, mm_camera );\r\n//\r\n//}\r\n\r\nmm.init();\r\n//render();\r\n","url":""},"play_close.js":{"hash":"43c_b3b644c60c4b2b08c5786d41157889b2","name":"play_close.js","data":"mmt.voxelRemoveEvents();\n\nconsole.log(mmt.objects.length);\n\nmmp.data = [];\nif(mmt.objects){\n    for(var i = mmt.length; i<mmt.objects.length; i++){\n        mmp.data.push(mmt.objects[i].position.toArray());\n    }\n}\n\nvar three_container = document.getElementById(\"three-container\");\n\nmm_camera = new THREE.OrthographicCamera( three_container.clientWidth / - 2, three_container.clientWidth / 2, three_container.clientHeight / 2, three_container.clientHeight / - 2, 1, 1000 );\n//}\n//t/mm_camera = new THREE.PerspectiveCamera( 75, box2.clientWidth / (box2.clientHeight), 1, 1000 );\n//mm_camera = new THREE.OrthographicCamera( three_container.clientWidth / - 2, three_container.clientWidth / 2, three_container.clientHeight / 2, three_container.clientHeight / - 2, 1, 1000 );\n//mm_camera.position.z = 500;\nmm_camera.position.y = 200;\nmm_camera.position.x = 200;\nmm_camera.position.z = 100;\nmm_camera.up = new THREE.Vector3( 0, 0, 1 );\nmm_camera.lookAt(new THREE.Vector3( 0, 0, 0 ));\n\nmm_clock = new THREE.Clock();\nmm_controls = new THREE.OrbitControls( mm_camera, mm_renderer.domElement );\n","url":""}}PK
     �t�L            	   textures/PK
     �t�LC���         textures/metadata.json{}PK
     �t�L               objects/PK
     �t�LC���         objects/metadata.json{}PK 
     �t�L�Q��S   S                    mmpPK 
     �t�L����m  m               t   xmlPK 
     �t�L��E)�"  �"                 jsPK 
     �t�Lc���E#  E#  	             �%  functionsPK 
     �t�L            	            I  textures/PK 
     �t�LC���                   5I  textures/metadata.jsonPK 
     �t�L                        kI  objects/PK 
     �t�LC���                   �I  objects/metadata.jsonPK      �  �I    