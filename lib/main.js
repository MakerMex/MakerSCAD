/**
 * Copyright (c) 2018 Juan Carlos Orozco Arena. 
 * The MakerSCAD trademark, name and the block icons are owned and Copyright (c) 2018 MakerMex, SA de CV.
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License V3.0 as published by
 * the Free Software Foundation
 *
 * Author: Juan Carlos Orozco
 * Includes contributions by MakerMex team: Luis Arturo Pacheco

 * 3D design software based on software distributed with MIT License:
 * Blockly
 * Threejs
 * Polymer 
 */

// http://stackoverflow.com/questions/16308779/how-can-i-hide-show-a-div-when-a-button-is-clicked

// init() and animate() are on the bottom of mm_onload

var blockly_xml = "";
var blockly_code = "";

var mm_console1;

function mm_console1_clear(){
  mm_console1.value = '';
}

var console1 = {
  log: function(msg){
    mm_console1.value += msg
  },
  logln: function(msg){
    mm_console1.value += msg + '\n'
  },
  clear: function(){
    mm_console1.value = ''  
  }
}

//var outln = function(msg){
//  out1.innerHTML += msg+'<br>';
//}
//
//var out = function(msg){
//  out1.innerHTML += msg;
//}

var mm_onload = function(){
  try{
    // Add event listeners to loadXML files
    var importFile = document.getElementById('import-sc-file');
    //importFile.addEventListener('change', importXML, false);
    importFile.addEventListener('change', _import, false);

    mm_console1 = document.getElementById("console1");

    var loader = new THREE.FontLoader();
    // TODO: Update threejs
    loader.load( 'fonts/helvetiker_regular.typeface.json', function ( font ) {
    //    loader.load( 'fonts/helvetiker_regular.typeface.js', function ( font ) {    
      initThreejs( font );
      //animate();
    });
    //init();
    // animate(); // I placed this on drawClick function

    // scene = new THREE.Scene();
    //
    // geometry = new THREE.CubeGeometry( 50, 200, 200 );
    // material = new THREE.MeshBasicMaterial( { color: 0x000000, wireframe: true, wireframeLinewidth: 2 } );
    //
    // mesh = new THREE.Mesh( geometry, material );
    // scene.add( mesh );
  } catch(err) {
    console.error(err);
  }
}
var mm_init_objects = function(){
  if(mm.mqtt.length > 0){
    for(mq of mm.mqtt){
        mq.end();
    }
  }
  mm.mqtt=[]
  if(mm.gui){
    mm.gui.destroy();
  }
  mm.gui = null;
  // TODO JCOA: Destroy all created meshes to clean un memory. (Is this necessary?)
}
var drawClick = function (event) {
  try{
    //var blockly = document.querySelector("element-blockly");

    mm_init_objects();
    console1.clear(); // Clear console1 on each draw.

    var src = this.blockly_code;
    displayCode(this.blockly_code);
    animate();
  } catch(err) {
    console.error(err);
  }
}
var displayCode = function (code) {
  try{
    if(code !== ""){
      if(mm_three_toolbar.grid) code += "\nmm_scene.add(mm_grid)"; // "\nmm_scene.add(new THREE.GridHelper( 200, 10 ))";
      if(mm_three_toolbar.axis) code += "\nmm_scene.add(new THREE.AxisHelper( 30 ));";
      // if(mm_three_toolbar.orthographic) {
      //   code += "\nmm_camera.toOrthographic();";}// to do correct responsiveness and compatibilitu with orbit controls
      //   else{
      //     code += "\nmm_camera.toPerspective();";
      //   }
    //console.log(code);
    } 
  } catch(err) {
    console.error(err);
  }
  try {
    eval(code);
  } catch (e) {
    //console.error(e);
    console1.log("Error: ");
    console1.logln(e);
  }
}
var newClick = function(event){
  mm_assets_textures = {};
  mm_assets_objects = {};
  mm_assets_functions = {};
  blockly_xml = "";
  blockly_code = "";
  if (Blockly.mainWorkspace !== null) {
    Blockly.mainWorkspace.clear();
  }
  for( var i = mm_scene.children.length - 1; i >= 0; i--) {
    obj = mm_scene.children[i];
    mm_scene.remove(obj);
  }
  
  location.reload();
  //drawClick(); // TODO: Clear threejs window.
  //blockly_reload();
}
var debugClick = function(event){
  console1.clear();
  console1.logln(mm_console);
  //require('remote').getCurrentWindow().toggleDevTools();
  //let mainWindow;
  //function createWindow () {
  //mainWindow = new BrowserWindow({width: 800, height: 600})
  //window.webContents.openDevTools();
}
var _newTexture = function(image){
  var texture = new THREE.Texture();
  texture.image = image;
  texture.needsUpdate = true;
  return texture;
}
var _add_textures_to_zip = function(zip){
  var metadata = mm_repository.getMetadata(mm_assets_textures);
  zip.file("textures/metadata.json", JSON.stringify(metadata));
  Object.keys(mm_assets_textures).forEach( function(key) {
      //console.log("iterating over", key);
      //console.log(this);
      // if(typeof this[key].url == "undefined"){
      //   var data = this[key].texture.image.src;
      // }
      // else{
      //   var data = url;
      // }
      var image = this[key];
      var data;

      if(image.url!=""){
        data = {url: image.url, name: image.name, data: image.data.image.src};
      }
      else{
        //data = {url: image.url, name: image.name, data: image.data.image.src};
        data = image.data.image.src;
      }
      zip.file("textures/"+key+"_"+image.name, JSON.stringify(data));
    }, mm_assets_textures);
  return zip;
}
var _get_textures_from_zip = function(zip){
  var loader = new THREE.ImageLoader();
  //console.log("_get_textures_from_zip");
  zip.file("textures/metadata.json").async("string").then(function (data) {
    var metadata = JSON.parse(data);
    Object.keys(metadata).forEach(function (id){
      var attr = metadata[id];
      if(attr.url!=""){
        //_loadObjectFromURL(attr.url, this, false);
        loader.load(
          attr.url,
          function ( image ) {
            var texture = _newTexture(image);
            mm_assets_textures[attr.url] = {data: texture,
              name: attr.name,
              url: attr.url};
          },
          function(progress){
          },
          function (err) {
            console.error(err);
          }
        );
      }
      else{
        zip.file("textures/"+id+"_"+attr.name).async("string").then(function (data) {
          var image = new Image();
          image.src = JSON.parse(data);
          var texture = _newTexture(image);
          // TODO: Save and Load also the name and url if applicable.
          mm_assets_textures[id] = {url: attr.url, name: attr.name, data: texture};
        });
      }
    });
  });
}

var _loadObjectFromURL = function(url, that, draw){
  var urlParts = mm_repository._urlParts(url);
  switch(urlParts.ext){
    case "stl":
      var loader = new THREE.STLLoader();
      loader.load(
        url,
        function ( object ) {
          if(draw){
            mm_repository._setAssetDraw(that, mm_assets_objects, url, mm_new_mesh(object), urlParts.name, url);
          }
          else{
            mm_repository._setAsset(mm_assets_objects, url, mm_new_mesh(object), urlParts.name, url);
          }
          // mm_assets_objects[name] = mm_new_mesh(object);
          // that._drawAsset(mm_assets_objects[name]);
          // // TODO: Check if this is enough information to distinguish a url from a file descriptor.
          // var urlDescr = {"url": url, "name": name};
          // that.selected = name;
          // that.push("assets", urlDescr);
        },
        function(progress){
        },
        function (err) {
          console.error(err);
        }
      );
      break;
    case "obj":
      var loader = new THREE.OBJLoader();
      loader.load(
        url,
        function ( object ) {
          if(draw){
            mm_repository._setAssetDraw(that, mm_assets_objects, url, mm_set(object), urlParts.name, url);
          }
          else {
            mm_repository._setAsset(mm_assets_objects, url, mm_set(object), urlParts.name, url);
          }
          // mm_assets_objects[name] = mm_set(object);
          // that._drawAsset(mm_assets_objects[name]);
          // // TODO: Check if this is enough information to distinguish a url from a file descriptor.
          // var urlDescr = {"url": url, "name": name};
          // that.selected = name;
          // that.push("assets", urlDescr);
        },
        function(progress){
        },
        function (err) {
          console.error(err);
        }
      );
      break;
    default:
      alert("URL must end with .stl or .obj");
      break;
  }
}

var _add_objects_to_zip = function(zip){
  var exporter = new THREE.OBJExporter();

  var metadata = mm_repository.getMetadata(mm_assets_objects);
  zip.file("objects/metadata.json", JSON.stringify(metadata));
  Object.keys(mm_assets_objects).forEach( function(key) {
      //console.log("iterating over", key);
      //var str = JSON.stringify(this[key]);
      //var str = this[key].toJSON();

      var obj = this[key];
      var data;

      if(obj.url!=""){
        //data = {url: obj.url, name: obj.name, data: obj.ur};
        data = {url: obj.url, name: obj.name};
      }
      else{
        var objString = exporter.parse(obj.data); // scene);
        var blob = new Blob([objString], {
            type : 'text/plain'
          });

        //var data = {url: obj.url, name: obj.name, data: blob};
        //data = {url: obj.url, name: obj.name, data: objString};
        //data = {url: obj.url, name: obj.name};
        //console.log(str);
        zip.file("objects/"+key+"_"+obj.name, blob); // str); //key); //this[key].toJSON());
      }
    }, mm_assets_objects);
    return zip;
}
var _get_objects_from_zip = function(zip){
  //var loader = new THREE.JSONLoader();
  //var loader = new THREE.ObjectLoader();

  //var loader = new THREE.OBJLoader();
  //console.log("_get_objects_from_zip");
  // First get ids:
  zip.file("objects/metadata.json").async("string").then(function (data) {
    var metadata = JSON.parse(data);
    var objLoader = new THREE.OBJLoader();

    // TODO: Do we need to init the repository?
    Object.keys(metadata).forEach(function (id){
      var attr = metadata[id];
      if(attr.url!=""){
        _loadObjectFromURL(attr.url, this, false);
      }
      else{
        zip.file("objects/"+id+"_"+attr.name).async("string").then(function (data) {
          try{
            var model = objLoader.parse(data);
          }
          catch(err){
            console.error(err);
          }
          mm_assets_objects[id] = {
            url: attr.url,
            name: attr.name,
            data: mm_set(model)
          };
        });
      }
    });
  });
}
var _buildZip = function(){
  var zip = new JSZip();

  var xml = "";
  if(blockly_xml != ""){
    var xml = Blockly.Xml.domToText(blockly_xml);
  }
  zip.file("mmp", JSON.stringify(mmp)); // Persistance user storage
  zip.file("xml", xml);
  zip.file("js", this.blockly_code);
  zip.file("functions", JSON.stringify(mm_assets_functions));
  zip = _add_textures_to_zip(zip);
  zip = _add_objects_to_zip(zip);

  return zip;
}
var saveClick = function (event) {
  // Using localstorage:
  if(typeof(Storage) !== "undefined") {
    var zip = _buildZip();
    zip.generateAsync({type:"string"}) // base64 (needs base64: true on loadAsync options) OK // uint8array X // string (no options on loadAsync) OK // blob X
      .then(function(blob){
        localStorage.setItem("zip", blob);
      });
    //localStorage.setItem("bundle", JSON.stringify(store)); // Cryo.stringify(store));
  } else {
    console.log("Sorry! No web storage support.");
  }
}
var _loadZip = function(str){
  // TODO: Check if we are clearing the repositories before loading the new assets
  var zip = new JSZip();
  zip.loadAsync(str) //, {base64: true})
    .then(function(zip){
      try{
        zip.file("xml").async("string")
          .then(function(xml){
            blockly_xml = xml;
            blockly_reload();
          });
        zip.file("functions").async("string")
          .then(function(functions){
            mm_assets_functions = JSON.parse(functions);
          });
        _get_textures_from_zip(zip);
        _get_objects_from_zip(zip);
        zip.file("mmp").async("string")
          .then(function(mmp1){
            mmp = JSON.parse(mmp1);
          });      
      }
      catch(err){
        console.error(err);
      }
      // drawClick(); // This is not working, it appears there is a time sync issue (due to async load maybe). 
    });
}
var loadClick = function (event) {
  //var blockly = document.querySelector("element-blockly");
  if(typeof blockly_xml !== "undefined"){
    //console.log(blockly_xml);
  }
  // Using localstorage:
  if(typeof(Storage) !== "undefined") {
    // TODO: Maybe clear workspace before loading.
    // blockly.xml = Blockly.Xml.domToText(blockly_xml); // Update xml to property to make sure that load has a change and fires.
    blockly_xml = ""; // Make sure load fires every time.
    //var xml = localStorage.getItem("blockly_code");

    _loadZip(localStorage.getItem("zip"));

    //var store = JSON.parse(localStorage.getItem("bundle")); // Cryo.parse(localStorage.getItem("bundle"));

    // Todo add xml property to blockly element.
    //blockly_xml = zip.file("xml");  //blockly_xml = store.xml;
    //mm_assets_functions = store.functions;
    //mm_assets_textures = store.textures;
    //mm_assets_objects = store.objects;

    //console.log(blockly_xml);
    //blockly_reload();
  } else {
    console.log("Sorry! No web storage support.");
  }
}

var _import = function (event) {
  var input = event.target;
  //console.log("import "+input.files[0]);
  var reader = new FileReader();
  reader.onload = function(){
    _loadZip(reader.result);
  }
  reader.readAsArrayBuffer(input.files[0]);
  event.target.value = ""; // Allow _fileChange fire even if it is the same file ahain (updated file or a deleted asset)
  //drawClick();
}
var importClick = function (event) {
  var importFile = document.getElementById('import-sc-file');
  importFile.click();
}

var importXML = function (event) {
  var input = event.target;
  //console.log("importXML "+input.files[0]);
  var reader = new FileReader();
  reader.onload = function(){
    blockly_xml = reader.result;
    try{
      var dom = Blockly.Xml.textToDom(blockly_xml);
      // Clear workspace first:
      Blockly.mainWorkspace.clear();
      Blockly.Xml.domToWorkspace(Blockly.mainWorkspace, dom);
      //loadXML(dom);
    }
    catch(err) {
      console.error(err);
    }
  };
  reader.readAsText(input.files[0]);
}
var importXMLCLick = function (event) {
  var importFile = document.getElementById('import-file');
  importFile.click();
  //$('#file').trigger('click');
  //    });
  //document.getElementById('file').addEventListener('change', readFile, false);
  //});
}

// JCOA: For browser that do not support saveAs: https://github.com/eligrey/FileSaver.js
var saveSTLBinary = function (scene, name) {
  var exporter = new THREE.STLBinaryExporter();
  var stlString = exporter.parse(scene);
  var blob = new Blob([stlString], {
      type : 'text/plain'
    });
  saveAs(blob, name + '.stl');
}
var exportSTLClick = function (event) {
  saveSTLBinary(mm_scene, "Test1");
}

var saveOBJ = function (scene, name) {
  var exporter = new THREE.OBJExporter();
  var objString = exporter.parse(scene);

  var blob = new Blob([objString], {
      type : 'text/plain'
    });
  saveAs(blob, name + '.obj');
}
var exportOBJClick = function (event) {
  saveOBJ(mm_scene, "Test1");
}

var saveJS = function (name) {
  var code="";
  getCode();
  if(typeof blockly_code != 'undefined'){
    code = blockly_code;
  }
  var blob = new Blob([code], {
      type : 'text/plain'
    });
  saveAs(blob, name + '.js');
}
var exportJSClick = function (event) {
  saveJS("Test1");
}

var saveHTML = function (name) {
  var code="";
  getCode();
  if(typeof blockly_code != 'undefined'){
    code = blockly_code;
  }
  var html = ''+
  '<!DOCTYPE html>\n'+
  '<html lang="en">\n'+
  '	<head>\n'+
  '		<title>MakerSCAD HTML Export</title>\n'+
  '		<meta charset="utf-8">\n'+
  '		<meta name="viewport" content="width=device-width, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">\n'+
  '		<style>\n'+
  '			body {\n'+
  '				margin: 0px;\n'+
  '				background-color: #000000;\n'+
  '				overflow: hidden;\n'+
  '			}\n'+
  '		</style>\n'+
  '	</head>\n'+
  '	<body>\n'+
  '    <div id="three-container">\n'+
  '     <div id="three" style="width:100vw; height:100vh;"></div>\n'+
  '    </div>\n'+
  '    <div id="console1"></div>'+
  ' <script src="https://rawgit.com/mrdoob/three.js/master/build/three.js"></script>\n'+
  ' <script src="https://rawgit.com/mrdoob/three.js/master/examples/js/controls/OrbitControls.js"></script>\n'+
  ' <script src="https://rawgit.com/mrdoob/three.js/master/examples/js/controls/TrackballControls.js"></script>\n'+
  ' <script src="https://rawgit.com/mrdoob/three.js/master/examples/js/cameras/CombinedCamera.js"></script>\n'+
  ' <script src="https://rawgit.com/mrdoob/three.js/master/examples/js/loaders/OBJLoader.js"></script>\n'+
  ' <script src="https://rawgit.com/mrdoob/three.js/master/examples/js/loaders/STLLoader.js"></script>\n'+
  ' <script src="https://rawgit.com/mrdoob/three.js/master/examples/js/exporters/STLBinaryExporter.js"></script>\n'+
  ' <script src="https://rawgit.com/mrdoob/three.js/master/examples/js/exporters/OBJExporter.js"></script>\n'+
  ' <script src="https://rawgit.com/mrdoob/three.js/master/examples/js/modifiers/SubdivisionModifier.js"></script>\n'+
  ' <script src="https://rawgit.com/mrdoob/three.js/master/examples/js/Detector.js"></script>\n'+
  ' <script src="https://rawgit.com/chandlerprall/ThreeCSG/master/ThreeCSG.js"></script>\n'+
  ' <script src="https://rawgit.com/chandlerprall/Physijs/master/physi.js"></script>\n'+
  //' <script src="https://rawgit.com/chandlerprall/Physijs/master/physijs_worker.js"></script>\n'+
  ' <script src="https://rawgit.com/JC-Orozco/mm_threejs/master/mm_three.js"></script>\n'+
  ' <script>\n'+
  '\n'+
  '   var mm_three_toolbar = {wireframe: false};\n'+
  '   var mm_state = {animate: false};\n'+
  '\n'+
  '   var loader = new THREE.FontLoader();\n'+
  '			loader.load( "https://rawgit.com/mrdoob/three.js/master/examples/fonts/helvetiker_regular.typeface.json", function ( mm_font ) {\n'+
  '\n'+
  '				initThreejs( mm_font );\n'+
  '\n'+
  code+
  '\n'+
  '				animate();\n'+
  '\n'+
  '			} );\n'+
  '		</script>\n'+
  '	</body>\n'+
  '</html>\n';

  var blob = new Blob([html], {
      type : 'text/plain'
    });
  saveAs(blob, name + '.html');
}
var exportHTMLClick = function (event) {
  saveHTML("Test1");
}

// TODO JCOA: Merge saveHTML and saveVRHTML adding an optional vr parameter (set to true)
var saveVRHTML = function (name) {
  var code="";
  getCode();
  if(typeof blockly_code != 'undefined'){
    code = blockly_code;
  }
  var html = ''+
  '<!DOCTYPE html>\n'+
  '<html lang="en">\n'+
  '	<head>\n'+
  '		<title>MakerSCAD HTML Export</title>\n'+
  '		<meta charset="utf-8">\n'+
  '		<meta name="viewport" content="width=device-width, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">\n'+
  '		<style>\n'+
  '			body {\n'+
  '				margin: 0px;\n'+
  '				background-color: #000000;\n'+
  '				overflow: hidden;\n'+
  '			}\n'+
  '		</style>\n'+
  '	</head>\n'+
  '	<body>\n'+
  '    <div id="three-container">\n'+
  '     <div id="three" style="width:100vw; height:100vh;"></div>\n'+
  '    </div>\n'+
  '    <div id="console1"></div>'+
  ' <script src="https://rawgit.com/mrdoob/three.js/master/build/three.js"></script>\n'+
  ' <script src="https://rawgit.com/mrdoob/three.js/master/examples/js/controls/OrbitControls.js"></script>\n'+
  ' <script src="https://rawgit.com/mrdoob/three.js/master/examples/js/effects/StereoEffect.js"></script>\n'+
  ' <script src="https://rawgit.com/mrdoob/three.js/master/examples/js/controls/DeviceOrientationControls.js"></script>\n'+ 
  ' <script src="https://rawgit.com/mrdoob/three.js/master/examples/js/controls/TrackballControls.js"></script>\n'+
  ' <!-- <script src="https://rawgit.com/mrdoob/three.js/master/examples/js/cameras/CombinedCamera.js"></script> -->\n'+
  ' <script src="https://rawgit.com/mrdoob/three.js/master/examples/js/loaders/OBJLoader.js"></script>\n'+
  ' <script src="https://rawgit.com/mrdoob/three.js/master/examples/js/loaders/STLLoader.js"></script>\n'+
  ' <script src="https://rawgit.com/mrdoob/three.js/master/examples/js/exporters/STLBinaryExporter.js"></script>\n'+
  ' <script src="https://rawgit.com/mrdoob/three.js/master/examples/js/exporters/OBJExporter.js"></script>\n'+
  ' <script src="https://rawgit.com/mrdoob/three.js/master/examples/js/modifiers/SubdivisionModifier.js"></script>\n'+
  ' <script src="https://rawgit.com/mrdoob/three.js/master/examples/js/Detector.js"></script>\n'+
  ' <script src="https://rawgit.com/chandlerprall/ThreeCSG/master/ThreeCSG.js"></script>\n'+
  ' <script src="https://rawgit.com/chandlerprall/Physijs/master/physi.js"></script>\n'+
  //' <script src="https://rawgit.com/chandlerprall/Physijs/master/physijs_worker.js"></script>\n'+
  ' <script src="https://rawgit.com/JC-Orozco/mm_threejs/master/mm_three.js"></script>\n'+
  ' <script>\n'+
  '\n'+
  '   var mm_three_toolbar = {wireframe: false};\n'+
  '   var mm_state = {animate: false};\n'+
  '\n'+
  '   var loader = new THREE.FontLoader();\n'+
  '			loader.load( "https://rawgit.com/mrdoob/three.js/master/examples/fonts/helvetiker_regular.typeface.json", function ( mm_font ) {\n'+
  '\n'+
  '				initVR( mm_font );\n'+
  '\n'+
  code+
  '\n'+
//  '				animate();\n'+
//  '\n'+
  '			} );\n'+
  '		</script>\n'+
  '	</body>\n'+
  '</html>\n';

  var blob = new Blob([html], {
      type : 'text/plain'
    });
  saveAs(blob, name + '.html');
}
var exportVRHTMLClick = function (event) {
  saveVRHTML("VR_Test1");
}


var _save = function (name) {
  var zip = _buildZip();
  zip.generateAsync({type:"blob"}) // base64 (needs base64: true on loadAsync options) OK // uint8array X // string (no options on loadAsync) OK // blob X
    .then(function(blob){
      saveAs(blob, name + '.mc');
    });
  //saveAs("ABC", name + '.sc2.zip');
}
var exportClick = function(){
  _save("Test1");
}

var saveXML = function (name) {
  if(typeof blockly_xml != 'undefined'){
    if(blockly_xml==""){
      var blob = new Blob([""], {
          type : 'text/plain'
        });
    }
    else {
      var blob = new Blob([Blockly.Xml.domToPrettyText(blockly_xml)], {
          type : 'text/plain'
        });
    }
    saveAs(blob, name + '.xml');
  }
}
var exportXMLClick = function (event) {
  saveXML("Test1");
}

var aboutClick = function (event) {
  //console.log("About");
  var dialog = document.getElementById("about1");
  if (dialog) {
    dialog.open();
  }
}
// var closeAbout = function (event) {
//     var dialog = document.getElementById("about1");
//     if (dialog) {
//       dialog.close();
//     }
//   };
var polygonClick = function (event) {
  //console.log("Polygon Editor");
  var dialog = document.getElementById("polygon-editor-dialog");

  if (dialog) {
    dialog.open();
    document.getElementById('polygon-editor').src += '';
    // polygon_editor.location.reload();
  }
}

var closePEClick = function (event) {
  var dialog = document.getElementById("polygon-editor-dialog");
  if (dialog) {
    var pEditor = document.getElementById("polygon-editor").contentWindow; // id = exportArea
    var eArea = pEditor.$("#exportArea");
    var polygon = eArea[0].value;
    var polygon1 = "";
    if(polygon.startsWith("polygon(")){
      var n = polygon.indexOf(")");
      if(n>8){
        polygon1=polygon.substr(8, n-8); // JCOA TODO: Change 10 to the place of the first ")"
      }
    }
    if(typeof Blockly.PolygonMutator.currentFieldTextInput !== "undefined"){
      Blockly.PolygonMutator.currentFieldTextInput.setValue(polygon1);
    }
    dialog.close();
  }
}

var closePlayDialogClick = function (event) {
  var three_div = document.getElementById("three");
  var three_container = document.getElementById("three-container");
  mm_renderer.setSize( three_container.clientWidth, three_container.clientHeight );
  three_div.appendChild( mm_renderer.domElement );

  var dialog = document.getElementById("play-dialog");
  dialog.close();
}

// Blockly functions. TODO: Add blockly to the name of this functions to avoid global variable collisions
var updateWorkspace = function (e){
  //console.log("updateWorkspace");
  myUpdate();
  //this.code = Blockly.JavaScript.workspaceToCode(Blockly.mainWorkspace);
  //this.xml = Blockly.Xml.workspaceToDom(Blockly.mainWorkspace);
}

var getCode = function() {
    // TODO: We may be able to simplify this by using Blockly.mainWorkspace (instead of my_workspace) and make this code directly on the function that calls this.
    //console.log("getCode");
    //console.log(my_workspace);
    //console.log(Blockly.JavaScript);
    try{
      blockly_code = Blockly.JavaScript.workspaceToCode(Blockly.mainWorkspace); // "this" is Window
      editor.setValue(blockly_code, -1);
      blockly_xml = Blockly.Xml.workspaceToDom(Blockly.mainWorkspace);
      // TODO: JCOA Is it possible to check first if the program is valid (no errors) before drawing.
      if(mm_settings.auto) drawClick();
    } catch(err) {
      console.error(err);
    }
};

var myUpdate = function() {
    //console.log("myUpdate");
    getCode();
}

var blockly_reload = function (){
  //console.log("reload");
  if(blockly_xml!=""){
    try{
      var dom = Blockly.Xml.textToDom(blockly_xml);
      // Clear workspace first:
      Blockly.mainWorkspace.clear();
      Blockly.Xml.domToWorkspace(Blockly.mainWorkspace, dom);
      //loadXML(dom);
    }
    catch(err) {
      console.error(err);
    }
  }
}
