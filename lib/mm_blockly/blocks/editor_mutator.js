/**
 * @license
 * Visual Blocks Editor
 *
 * Copyright 2012 Google Inc.
 * https://developers.google.com/blockly/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @fileoverview Object representing a mutator dialog.  A mutator allows the
 * user to change the shape of a block using a nested blocks editor.
 * @author fraser@google.com (Neil Fraser)
 * Adapted by Juan Carlos Orozco to work with polygon block
 */
'use strict';

goog.provide('Blockly.EditorMutator');

goog.require('Blockly.Bubble');
goog.require('Blockly.Icon');
goog.require('Blockly.WorkspaceSvg');
goog.require('goog.Timer');
goog.require('goog.dom');

Blockly.EditorMutator.text_block = null;

/**
 * Class for a mutator dialog.
 * @param {!Array.<string>} quarkNames List of names of sub-blocks for flyout.
 * @extends {Blockly.Icon}
 * @constructor
 */
Blockly.EditorMutator = function(quarkNames) {
  Blockly.EditorMutator.superClass_.constructor.call(this, null);
  this.quarkNames_ = quarkNames;
};
goog.inherits(Blockly.EditorMutator, Blockly.Icon);

/**
 * Width of workspace.
 * @private
 */
Blockly.EditorMutator.prototype.workspaceWidth_ = 0;

/**
 * Height of workspace.
 * @private
 */
Blockly.EditorMutator.prototype.workspaceHeight_ = 0;

/**
 * Draw the mutator icon.
 * @param {!Element} group The icon group.
 * @private
 */
Blockly.EditorMutator.prototype.drawIcon_ = function(group) {
  // Square with rounded corners.
  Blockly.createSvgElement('rect',
      {'class': 'blocklyIconShape',
       'rx': '4', 'ry': '4',
       'height': '16', 'width': '16'},
       group);
  // Gear teeth.
  Blockly.createSvgElement('path',
      {'class': 'blocklyIconSymbol',
       'd': 'm4.203,7.296 0,1.368 -0.92,0.677 -0.11,0.41 0.9,1.559 0.41,0.11 1.043,-0.457 1.187,0.683 0.127,1.134 0.3,0.3 1.8,0 0.3,-0.299 0.127,-1.138 1.185,-0.682 1.046,0.458 0.409,-0.11 0.9,-1.559 -0.11,-0.41 -0.92,-0.677 0,-1.366 0.92,-0.677 0.11,-0.41 -0.9,-1.559 -0.409,-0.109 -1.046,0.458 -1.185,-0.682 -0.127,-1.138 -0.3,-0.299 -1.8,0 -0.3,0.3 -0.126,1.135 -1.187,0.682 -1.043,-0.457 -0.41,0.11 -0.899,1.559 0.108,0.409z'},
       group);
  // Axle hole.
  Blockly.createSvgElement('circle',
      {'class': 'blocklyIconShape', 'r': '2.7', 'cx': '8', 'cy': '8'},
       group);
};

/**
 * Clicking on the icon toggles if the mutator bubble is visible.
 * Disable if block is uneditable.
 * @param {!Event} e Mouse click event.
 * @private
 * @override
 */
Blockly.EditorMutator.prototype.iconClick_ = function(e) {
  if (this.block_.isEditable()) {
    Blockly.Icon.prototype.iconClick_.call(this, e);
  }
};

/**
 * Show or hide the mutator bubble.
 * @param {boolean} visible True if the bubble should be visible.
 */
Blockly.EditorMutator.prototype.setVisible = function(visible) {
  if (visible == this.isVisible()) {
    // No change.
    return;
  }
  if (visible) {
    //console.log("Show EditorMutator");
    var index = 1;
    var row = 0;
    // TODO: JCOA Search item by field id (name)
    Blockly.EditorMutator.currentFieldTextInput = this.block_.inputList[index].fieldRow[row];
    var editor_name = Blockly.EditorMutator.currentFieldTextInput.text_; // .textElement_.innerHTML
    //console.log(editor_name);
    // Blockly.EditorMutator.currentFieldTextInput.setValue("Hello"); // Test
    // polygon_editor
    // Add a callback after polygon editor closes! The call back will set the polygon text to the output.
    //var pEditor = document.getElementById("polygon-editor").contentWindow; // id = exportArea
    //var eArea = pEditor.$("#exportArea");
    // polygonClick(null);
    var dialog = document.getElementById("assetsfunctions1");

    if (dialog) {
      dialog.open();
      //document.getElementById('polygon-editor').src += ''; // JCOA: This resets polygon dialog window and does not allow value assignment below to work.
      // polygon_editor.location.reload();
    }

  } else {
    //console.log("Hide EditorMutator"); // This may not apply since this is closed by the close button on the polygon editor
  }
};

/**
 * Dispose of this mutator.
 */
Blockly.EditorMutator.prototype.dispose = function() {
  this.block_.mutator = null;
  Blockly.Icon.prototype.dispose.call(this);
};
