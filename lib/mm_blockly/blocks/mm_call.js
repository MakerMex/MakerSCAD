/**
 * Copyright (c) 2018 Juan Carlos Orozco Arena. 
 * The MakerSCAD trademark, name and the block icons are owned and Copyright (c) 2018 MakerMex, SA de CV.
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License V3.0 as published by
 * the Free Software Foundation
 *
 * Author: Juan Carlos Orozco
 * Includes contributions by MakerMex team: Luis Arturo Pacheco

 * 3D design software based on software distributed with MIT License:
 * Blockly
 * Threejs
 * Polymer 
 */

//'use strict';
//
//goog.provide('Blockly.Blocks.jc');
//
//goog.require('Blockly.Blocks');
//
//Blockly.Blocks['mm_call'] = {
//  /**
//   * Block for creating a list with any number of elements of any type.
//   * @this Blockly.Block
//   */
//  init: function() {
//    this.setHelpUrl(Blockly.Msg.LISTS_CREATE_WITH_HELPURL);
//    this.setColour(Blockly.Blocks.lists.HUE);
//    this.appendAddSubGroup(Blockly.Msg.LISTS_CREATE_WITH_INPUT_WITH, 'items',
//                           null,
//                           Blockly.Msg.LISTS_CREATE_EMPTY_TITLE);
//    this.itemCount_ = 1;
//    this.updateShape_();
//    this.setOutput(true, 'Array');
//    this.setTooltip(Blockly.Msg.LISTS_CREATE_WITH_TOOLTIP);
//  }
//};


//Blockly.Blocks['mm_call'] = {
//  init: function() {
//    this.appendDummyInput()
//        .appendField("call1");
//    this.appendValueInput("arg1")
//        .setCheck(null)
//        .appendField("arg1");
//    this.setOutput(true, null);
//    this.setTooltip('');
//    this.setHelpUrl('http://www.example.com/');
//  }
//};

//Blockly.Blocks['mm_call'] = {
//  init: function() {
//    this.appendDummyInput()
//        .appendField("call1");
//    this.appendValueInput("arg1")
//        .setCheck(null)
//        .appendField("arg1");
//    this.appendValueInput("arg2")
//        .setCheck(null)
//        .appendField("arg2");
//    this.setOutput(true, null);
//    this.setTooltip('');
//    this.setHelpUrl('http://www.example.com/');
//  }
//};

// JSON
//{
//  "type": "mm_call",
//  "message0": "call1 %1 arg1 %2 arg2 %3",
//  "args0": [
//    {
//      "type": "input_dummy"
//    },
//    {
//      "type": "input_value",
//      "name": "arg1"
//    },
//    {
//      "type": "input_value",
//      "name": "arg2"
//    }
//  ],
//  "output": null,
//  "tooltip": "",
//  "helpUrl": "http://www.example.com/"
//}

// Two possible solutions to the custom call block.
// Create a block with mutator and a editable checkbox.
// Create a block programatically from xml.
// It seems the answer is a combination of both.

// Example of two function call blocks
//<xml xmlns="http://www.w3.org/1999/xhtml">
//<block type="mm_function_call" id="gZ;1UP-pk~8f-x;:f`Y!" x="91" y="66"> 
//  <mutation items="1"></mutation>
//  <field name="NAME">method1</field>
//  <value name="items0">
//    <block type="math_number" id="#37iacl8L`n2RKRx21Tw">
//      <field name="NUM">0</field>
//    </block>
//  </value>
//</block>
//
//<block type="mm_function_call" id="s%fgVJsZf3O4S%~(0DwE" x="93" y="207">
//  <mutation items="2"></mutation>
//  <field name="NAME">method2</field>
//  <value name="items0">
//    <block type="math_number" id="+Y4HK6}Ay5*jn,oN3gNB">
//      <field name="NUM">0</field>
//    </block>
//  </value>
//  <value name="items1">
//    <block type="text" id="fGj5/K{_kYfcFbd~KJIB">
//      <field name="TEXT">test</field>
//    </block>
//  </value>
//</block>
//</xml>

//Blockly.Blocks['lists_create_with'] = {
//  /**
//   * Block for creating a list with any number of elements of any type.
//   * @this Blockly.Block
//   */
//  init: function() {
//    this.setHelpUrl(Blockly.Msg.LISTS_CREATE_WITH_HELPURL);
//    this.setColour(Blockly.Blocks.lists.HUE);
//    this.setMutator(new Blockly.Mutator(['lists_create_with_item']));
////    if (this.workspace.options.useMutators) {
////      this.setMutator(new Blockly.Mutator(['lists_create_with_item']));
////    } else {
////      this.appendAddSubGroup(Blockly.Msg.LISTS_CREATE_WITH_INPUT_WITH, 'items',
////                           null,
////                           Blockly.Msg.LISTS_CREATE_EMPTY_TITLE);
////    }
//    this.itemCount_ = 1;
//    this.updateShape_();
//    this.setOutput(true, 'Array');
//    this.setTooltip(Blockly.Msg.LISTS_CREATE_WITH_TOOLTIP);
//  },
//  /**
//   * Create XML to represent list inputs.
//   * @return {!Element} XML storage element.
//   * @this Blockly.Block
//   */
//  mutationToDom: function() {
//    var container = document.createElement('mutation');
//    container.setAttribute('items', this.itemCount_);
//    return container;
//  },
//  /**
//   * Parse XML to restore the list inputs.
//   * @param {!Element} xmlElement XML storage element.
//   * @this Blockly.Block
//   */
//  domToMutation: function(xmlElement) {
//    this.itemCount_ = parseInt(xmlElement.getAttribute('items'), 10);
//    this.updateShape_();
//  },
//  /**
//   * Populate the mutator's dialog with this block's components.
//   * @param {!Blockly.Workspace} workspace Mutator's workspace.
//   * @return {!Blockly.Block} Root block in mutator.
//   * @this Blockly.Block
//   */
//  decompose: function(workspace) {
//    var containerBlock = workspace.newBlock('lists_create_with_container');
//    containerBlock.initSvg();
//    var connection = containerBlock.getInput('STACK').connection;
//    for (var i = 0; i < this.itemCount_; i++) {
//      var itemBlock = workspace.newBlock('lists_create_with_item');
//      itemBlock.initSvg();
//      connection.connect(itemBlock.previousConnection);
//      connection = itemBlock.nextConnection;
//    }
//    return containerBlock;
//  },
//  /**
//   * Reconfigure this block based on the mutator dialog's components.
//   * @param {!Blockly.Block} containerBlock Root block in mutator.
//   * @this Blockly.Block
//   */
//  compose: function(containerBlock) {
//    var itemBlock = containerBlock.getInputTargetBlock('STACK');
//    // Count number of inputs.
//    var connections = [];
//    while (itemBlock) {
//      connections.push(itemBlock.valueConnection_);
//      itemBlock = itemBlock.nextConnection &&
//          itemBlock.nextConnection.targetBlock();
//    }
//    // Disconnect any children that don't belong.
//    for (var i = 0; i < this.itemCount_; i++) {
//      var connection = this.getInput('ADD' + i).connection.targetConnection;
//      if (connection && connections.indexOf(connection) == -1) {
//        connection.disconnect();
//      }
//    }
//    this.itemCount_ = connections.length;
//    this.updateShape_();
//    // Reconnect any child blocks.
//    for (var i = 0; i < this.itemCount_; i++) {
//      Blockly.Mutator.reconnect(connections[i], this, 'ADD' + i);
//    }
//  },
//  /**
//   * Store pointers to any connected child blocks.
//   * @param {!Blockly.Block} containerBlock Root block in mutator.
//   * @this Blockly.Block
//   */
//  saveConnections: function(containerBlock) {
//    var itemBlock = containerBlock.getInputTargetBlock('STACK');
//    var i = 0;
//    while (itemBlock) {
//      var input = this.getInput('ADD' + i);
//      itemBlock.valueConnection_ = input && input.connection.targetConnection;
//      i++;
//      itemBlock = itemBlock.nextConnection &&
//          itemBlock.nextConnection.targetBlock();
//    }
//  },
//  /**
//   * Modify this block to have the correct number of inputs.
//   * @private
//   * @this Blockly.Block
//   */
//  updateShape_: function() {
//    if (this.itemCount_ && this.getInput('EMPTY')) {
//      this.removeInput('EMPTY');
//    } else if (!this.itemCount_ && !this.getInput('EMPTY')) {
//      this.appendDummyInput('EMPTY')
//          .appendField(Blockly.Msg.LISTS_CREATE_EMPTY_TITLE);
//    }
//    // Add new inputs.
//    for (var i = 0; i < this.itemCount_; i++) {
//      if (!this.getInput('ADD' + i)) {
//        var input = this.appendValueInput('ADD' + i);
//        if (i == 0) {
//          input.appendField(Blockly.Msg.LISTS_CREATE_WITH_INPUT_WITH);
//        }
//      }
//    }
//    // Remove deleted inputs.
//    while (this.getInput('ADD' + i)) {
//      this.removeInput('ADD' + i);
//      i++;
//    }
//  }
//};
