/**
 * Copyright (c) 2018 Juan Carlos Orozco Arena. 
 * The MakerSCAD trademark, name and the block icons are owned and Copyright (c) 2018 MakerMex, SA de CV.
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License V3.0 as published by
 * the Free Software Foundation
 *
 * Author: Juan Carlos Orozco
 * Includes contributions by MakerMex team: Luis Arturo Pacheco

 * 3D design software based on software distributed with MIT License:
 * Blockly
 * Threejs
 * Polymer 
 */

Blockly.JavaScript['mm_text_append'] = function(block) {
  var text_variable = block.getFieldValue('variable');
  var value_text = Blockly.JavaScript.valueToCode(block, 'TEXT', Blockly.JavaScript.ORDER_ATOMIC);
  var code = text_variable+" = String("+text_variable+") + String("+value_text+");\n";
  return code;
};

Blockly.JavaScript['mm_xyz'] = function(block) {
  var text_x = block.getFieldValue('X');
  var text_y = block.getFieldValue('Y');
  var text_z = block.getFieldValue('Z');
  var code = '['+text_x+', '+text_y+', '+text_z+']';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['mm_list_xyz'] = function(block) {
  var value_x = Blockly.JavaScript.valueToCode(block, 'X', Blockly.JavaScript.ORDER_ATOMIC);
  var value_y = Blockly.JavaScript.valueToCode(block, 'Y', Blockly.JavaScript.ORDER_ATOMIC);
  var value_z = Blockly.JavaScript.valueToCode(block, 'Z', Blockly.JavaScript.ORDER_ATOMIC);
  var code = '['+value_x+', '+value_y+', '+value_z+']';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['mm_code_part'] = function(block) {
  var text_code = block.getFieldValue('code');
  var code = text_code;
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['mm_code_line'] = function(block) {
  var text_code = block.getFieldValue('code');
  var code = text_code+"\n";
  return code;
};

Blockly.JavaScript['mm_access_field'] = function(block) {
  var value_variable = Blockly.JavaScript.variableDB_.getName(block.getFieldValue('variable'), Blockly.Variables.NAME_TYPE);
  var text_field = block.getFieldValue('field');
  var value_input = Blockly.JavaScript.valueToCode(block, 'input', Blockly.JavaScript.ORDER_ATOMIC);
  var code = value_variable+"."+text_field+" = "+value_input+";\n";;
  return code;
};

Blockly.JavaScript['mm_set_to'] = function(block) {
  var text_code = block.getFieldValue('code');
  var value_input = Blockly.JavaScript.valueToCode(block, 'input', Blockly.JavaScript.ORDER_ATOMIC);
  var code = text_code+" = "+value_input+";\n";
  return code;
};

Blockly.JavaScript['mm_call'] = function(block) {
  // Create a list with any number of elements of any type.
  //var text_name = block.getFieldValue('NAME');
  var statements_chain = Blockly.JavaScript.statementToCode(block, 'chain');
  var codeArr = new Array(block.itemCount_-1);
  for (var n = 1; n < block.itemCount_; n++) {
    // code[n] = Blockly.JavaScript.valueToCode(block, 'ADD' + n,
    //     Blockly.JavaScript.ORDER_COMMA) || 'null';
    // TODO: Fix the naming on the AddSubGroup block and use code above
    codeArr[n-1] = Blockly.JavaScript.valueToCode(block, 'items' + n,
        Blockly.JavaScript.ORDER_COMMA) || 'null';
  }
  var chain = "";
  if(statements_chain != ""){
    chain = "\n  ."+statements_chain.trim();
  }
  //var code = text_name.substr(1, text_name.length-2) + '(' + codeArr.join(', ') + ')' + chain;
  var code = '(' + codeArr.join(', ') + ')' + chain;
  //return [code, Blockly.JavaScript.ORDER_ATOMIC];
  return code;
};

Blockly.JavaScript['mm_function_call'] = function(block) {
  // Create a list with any number of elements of any type.
  var text_name = block.getFieldValue('NAME');
  var statements_chain = Blockly.JavaScript.statementToCode(block, 'chain');
  var codeArr = new Array(block.itemCount_-1); // block.itemCount_);
  for (var n = 1; n < block.itemCount_; n++) {
    // code[n] = Blockly.JavaScript.valueToCode(block, 'ADD' + n,
    //     Blockly.JavaScript.ORDER_COMMA) || 'null';
    // TODO: Fix the naming on the AddSubGroup block and use code above
    codeArr[n-1] = Blockly.JavaScript.valueToCode(block, 'items' + n,
        Blockly.JavaScript.ORDER_COMMA) || 'null';
  }
  var chain = "";
  if(statements_chain != ""){
    chain = "\n  ."+statements_chain.trim();
  }
  //var code = text_name.substr(1, text_name.length-2) + '(' + codeArr.join(', ') + ')' + chain;
  var code = text_name + '(' + codeArr.join(', ') + ')' + chain;
  //return [code, Blockly.JavaScript.ORDER_ATOMIC];
  return code;
};

Blockly.JavaScript['mm_function_call_return'] = function(block) {
  // Create a list with any number of elements of any type.
  var text_name = block.getFieldValue('NAME');
  var statements_chain = Blockly.JavaScript.statementToCode(block, 'chain');
  var codeArr = new Array(block.itemCount_-1); // block.itemCount_);
  for (var n = 1; n < block.itemCount_; n++) {
    // code[n] = Blockly.JavaScript.valueToCode(block, 'ADD' + n,
    //     Blockly.JavaScript.ORDER_COMMA) || 'null';
    // TODO: Fix the naming on the AddSubGroup block and use code above
    codeArr[n-1] = Blockly.JavaScript.valueToCode(block, 'items' + n,
        Blockly.JavaScript.ORDER_COMMA) || 'null';
  }
  var chain = "";
  if(statements_chain != ""){
    chain = "\n  ."+statements_chain.trim();
  }
  //var code = text_name.substr(1, text_name.length-2) + '(' + codeArr.join(', ') + ')' + chain;
  var code = text_name + '(' + codeArr.join(', ') + ')' + chain;
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
  //return code;
};

Blockly.JavaScript['mm_function'] = function(block) {
  var text_args = block.getFieldValue('args');
  var statements_chain = Blockly.JavaScript.statementToCode(block, 'chain');
  var chain = statements_chain;
  //var code = text_name.substr(1, text_name.length-2) + '(' + codeArr.join(', ') + ')' + chain;
  var code = 'function(' + text_args + '){\n' + chain +'}';

  //return [code, Blockly.JavaScript.ORDER_NONE];
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
  //return code;
};

Blockly.JavaScript['mm_return'] = function(block) {
  var value_ret = Blockly.JavaScript.valueToCode(block, 'ret', Blockly.JavaScript.ORDER_ATOMIC);
  var code = 'return '+value_ret+'\n';
  //return [code, Blockly.JavaScript.ORDER_ATOMIC];
  return code;
};

Blockly.JavaScript['mm_var'] = function(block) {
  var text_var = block.getFieldValue('var');
  var value_val = Blockly.JavaScript.valueToCode(block, 'val', Blockly.JavaScript.ORDER_ATOMIC);
  var code = 'var '+text_var+' = '+value_val+';\n';
  //return [code, Blockly.JavaScript.ORDER_NONE];
  return code;
};

Blockly.JavaScript['mm_var_return'] = function(block) {
  var text_name = block.getFieldValue('NAME');
  var code = text_name;
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
  //return code;
};

Blockly.JavaScript['mm_new'] = function(block) {
  var statements_chain = Blockly.JavaScript.statementToCode(block, 'chain');
  var chain = statements_chain.trim();
  var code = "new "+chain;
  //var code = statements_chain;
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
  //return code;
};

Blockly.JavaScript['mm_field'] = function(block) {
  var text_name = block.getFieldValue('NAME');
  var statements_chain = Blockly.JavaScript.statementToCode(block, 'chain');
  var chain = "";
  if(statements_chain != ""){
    chain = "."+statements_chain.trim();
  }
  var code = text_name+chain;
  //return [code, Blockly.JavaScript.ORDER_ATOMIC];
  return code;
};

Blockly.JavaScript['mm_adaptor'] = function(block) {
  var statements_chain = Blockly.JavaScript.statementToCode(block, 'chain');
  var chain = statements_chain.trim();
  var code = chain;
  //var code = statements_chain;
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
  //return code;
};

Blockly.JavaScript['mm_statement'] = function(block) {
  var statements_chain = Blockly.JavaScript.statementToCode(block, 'chain');
  var chain = statements_chain.trim();
  var code = chain;
  //var code = statements_chain;
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
  //return code;
};

Blockly.JavaScript['mm_spread'] = function(block) {
  var value_name = Blockly.JavaScript.valueToCode(block, 'arg_array', Blockly.JavaScript.ORDER_ATOMIC);
  var code = '...'+value_name;
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
  //return [code, Blockly.JavaScript.ORDER_NONE];
  //return code;
};

Blockly.JavaScript['mm_script_text'] = function(block) {
  var text_name = block.getFieldValue('name');
  var code = mm_assets_functions[text_name].data+"\n"; // mm_new_mesh(mm_assets["'+text_name+'"])';
  if(typeof code == "undefined") code = "";
  return code;
};

Blockly.JavaScript['mm_print'] = function(block) {
  var value_msg = Blockly.JavaScript.valueToCode(block, 'msg', Blockly.JavaScript.ORDER_ATOMIC);
  var code = 'console1.log('+value_msg+');\n';
  //return [code, Blockly.JavaScript.ORDER_ATOMIC];
  //return [code, Blockly.JavaScript.ORDER_NONE];
  return code;
};

Blockly.JavaScript['mm_println'] = function(block) {
  var value_msg = Blockly.JavaScript.valueToCode(block, 'msg', Blockly.JavaScript.ORDER_ATOMIC);
  var code = 'console1.logln('+value_msg+');\n';
  //return [code, Blockly.JavaScript.ORDER_ATOMIC];
  //return [code, Blockly.JavaScript.ORDER_NONE];
  return code;
};

Blockly.JavaScript['mm_load_library'] = function(block) {
  var text_name = block.getFieldValue('name');
  var code = 'mm_load_library("'+text_name+'");\n';
  return code;
};

Blockly.JavaScript['mm_mqtt_init'] = function(block) {
  var value_name = block.getFieldValue('name');
  var value_ws = block.getFieldValue('ws');
  var code = 
    "var "+value_name+" = mqtt.connect('"+value_ws+"')\n"+
    'mm.mqtt.push('+value_name+')\n';
  //return [code, Blockly.JavaScript.ORDER_ATOMIC];
  //return [code, Blockly.JavaScript.ORDER_NONE];
  return code;
};

Blockly.JavaScript['mm_mqtt_on_connect'] = function(block) {
  var value_name = block.getFieldValue('name');
  var statements_chain = Blockly.JavaScript.statementToCode(block, 'chain');
  var chain = "";
  if(statements_chain != ""){
    chain = statements_chain.trim();
  }
  var code =
    value_name+".on('connect', function(){\n"+
    '  let mq1 = '+value_name+';\n'+
    '  '+chain+'\n'+
    '});\n';
  return code;
};

Blockly.JavaScript['mm_mqtt_subscribe'] = function(block) {
  var value_name = block.getFieldValue('name');
  var value_topic = block.getFieldValue('topic');
  var statements_chain = Blockly.JavaScript.statementToCode(block, 'chain');
  var chain = "";
  if(statements_chain != ""){
    chain = statements_chain.trim();
  }
  var code =
    value_name+".subscribe('"+value_topic+"')\n"+
    value_name+".on('message', function (topic, message){\n"+
    '  '+chain+'\n'+
    '});\n'
  return code;
};

Blockly.JavaScript['mm_mqtt_publish'] = function(block) {
  var value_name = block.getFieldValue('name');
  var value_topic = block.getFieldValue('topic');
  var value_msg = Blockly.JavaScript.valueToCode(block, 'msg', Blockly.JavaScript.ORDER_ATOMIC);
  var code = value_name+".publish('"+value_topic+"', "+value_msg+");\n";
  return code;
};

Blockly.JavaScript['mm_gui_init'] = function(block) {
  var code = 
    'mm.gui_params = {};\n'+
    'mm.gui = new dat.GUI();\n';
  return code;
};

//text.message = 'dat.gui3';
//mm.gui.add(text, 'message');
//text.message2 = 'message2';
//mm.gui.add(text, 'message2');
//text.speed = 0.6;
//mm.gui.add(text, 'speed', -5, 5);
//text.displayOutline = false;
//mm.gui.add(text, 'displayOutline');
//text.explode = function() { console1.logln("Explode!") };
//mm.gui.add(text, 'explode');

Blockly.JavaScript['mm_gui_message'] = function(block) {
  var value_name = block.getFieldValue('name');
  var value_value = block.getFieldValue('value');
  var statements_chain = Blockly.JavaScript.statementToCode(block, 'chain');
  var chain = "";
  if(statements_chain != ""){
    chain = statements_chain.trim();
  }
  var code = 
    "mmp.ui."+value_name+' = mmp.ui.'+value_name+" || 'Hello';\n"+
    "mm.gui_params."+value_name+" = mmp.ui." + value_name + ";\n"+
    "mm.gui.add(mm.gui_params, '" + value_name + "')\n"+
    "  .onFinishChange(function (val){\n"+
    '    mmp.ui.'+value_name+' = val;\n'+
    '    '+chain+'\n'+
    '  });\n';
  return code;
};

Blockly.JavaScript['mm_gui_value'] = function(block) {
  var value_name = block.getFieldValue('name');
//  var value_value = block.getFieldValue('value');
  var value_min = block.getFieldValue('min');
  var value_max = block.getFieldValue('max');
  var value_step = block.getFieldValue('step');
  var statements_chain = Blockly.JavaScript.statementToCode(block, 'chain');
  var chain = "";
  if(statements_chain != ""){
    chain = statements_chain.trim();
  }
  var code =
    "mmp.ui."+value_name+' = mmp.ui.'+value_name+' || 0.0;\n'+
    "mm.gui_params."+value_name+" = mmp.ui." + value_name + ";\n"+
    "mm.gui.add(mm.gui_params, '" + value_name + "', "+value_min+", "+value_max+").step("+value_step+")\n"+
    "  .onChange(function (val){\n"+
    '    mmp.ui.'+value_name+' = val;\n'+
    '    '+chain+'\n'+
    '  });\n';
  return code;
};

Blockly.JavaScript['mm_gui_color'] = function(block) {
  var value_name = block.getFieldValue('name');
  var value_color = block.getFieldValue('color');
  var statements_chain = Blockly.JavaScript.statementToCode(block, 'chain');
  var chain = "";
  if(statements_chain != ""){
    chain = statements_chain.trim();
  }
  var code = 
    "mm.gui_params."+value_name+" = '" + value_color + "';\n"+
    "mm.gui.addColor(mm.gui_params, '" + value_name + "')\n"+
    "  .onChange(function (color){\n"+
    '    '+chain+'\n'+
    '  });\n';
  return code;
};

