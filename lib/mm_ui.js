/**
 * Copyright (c) 2018 Juan Carlos Orozco Arena. 
 * The MakerSCAD trademark, name and the block icons are owned and Copyright (c) 2018 MakerMex, SA de CV.
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License V3.0 as published by
 * the Free Software Foundation
 *
 * Author: Juan Carlos Orozco
 * Includes contributions by MakerMex team: Luis Arturo Pacheco

 * 3D design software based on software distributed with MIT License:
 * Blockly
 * Threejs
 * Polymer 
 */

// Define namespace
// window.MM_UI = window.MM_UI || {};
var MM_UI = MM_UI || {};

//MM_UI.
